<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  	<meta name="viewport" content="width=device-width" />
    <title>M-Mentoring</title>
    <!-- Bootstrap core CSS -->
  	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
  	<!-- Animation library for notifications -->
  	<link href="<?php echo base_url()?>assets/css/animate.min.css" rel="stylesheet"/>
  	<!-- Light Bootstrap Table core CSS -->
  	<link href="<?php echo base_url()?>assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <!-- Fonts and icons -->
  	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  	<link href="<?php echo base_url()?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
  	<!-- Js Script -->
  	<script src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
  </head>

  <style media="screen">
    .card-login{
      box-shadow: 0 25px 30px -13px rgba(40, 40, 40, 0.4);
      border-radius: 10px;
      padding-top: 10px;
      padding-bottom: 30px;
    }

    .auth-title h1{
      font-weight: bold;
    }
    .auth-title h1, 
    .auth-title h2, 
    .auth-title h3{
      color: #fff;
      text-shadow: .1em .1em .2em rgba(0, 0, 0, 0.4);
    }

    .full-page-background, .full-black {
      position: absolute;
      z-index: -1;
      height: 100%;
      width: 100%;
      display: block;
      top: 0;
      left: 0;
      background-size: cover;
      background-position: center center;
    }

    .full-black{
      background: linear-gradient(to bottom, #797979 0%, #797979 100%);
      opacity: .66;
    }
  </style>

  <body style="background: rgba(203, 203, 210, 0.15);">

    <div class="full-page-background"
      style="background-image: url(<?=base_url()?>/assets/img/full-screen-image-3.jpg) "></div>
    <div class="full-black"></div>

    <div class="container-fluid">

      <div class="col-md-3 col-sm-6 col-xs-12 pull-right" style="margin-top: 5%;">

        <form action="<?=site_url('Auth/masuk')?>" method="post">
          <div class="card card-login">
            <div class="header">
              <h3 class="header text-center">Silahkan Masuk Untuk Melanjutkan</h3>
            </div>
            <div class="content">
              <div class="content">
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" placeholder="Username" autofocus>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
              </div>

              <div class="text-center">
                <button type="submit" class="btn btn-warning btn-wd btn-fill">Login</button>
              </div>
            </div>
          </div>
        </form>

      </div>

      <div class="col-md-9 col-sm-6 col-xs-12 auth-title" style="margin-top: 5%;">
        <h1>M-Mentoring</h1>
        <h2>Sistem Mentoring Lembaga Dakwah Kampus</h2>
        <h3>Universitas Muhammadiyah Malang</h3>
      </div>

    </div>

  </body>
</html>
