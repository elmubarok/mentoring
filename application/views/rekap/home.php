<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #ababab;
    width: 50px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
    vertical-align: middle;
  }
  .pagination{
    margin-top: 5px;
  }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]:after{ display:none; }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]{ padding-right: 0px;}
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <button class="btn btn-sm btn-default btn-sw <?=(!isset($_GET['pos'])) ? 'active' : '';?>" data-tipe="my">Per-Bulan/Tahun</button>
          <button class="btn btn-sm btn-default btn-sw <?=(isset($_GET['pos'])) ? 'active' : '';?>" data-tipe="tgl">Per-Harian</button>
          <button class="btn btn-sm btn-danger pull-right" id="btn-destroy">
            <i class="fa fa-remove"></i>&nbsp;Hapus Semua
          </button>
        </div>
      </div>
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px;">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px;">No</th>
              <th>Kader</th>
              <th>Aktifitas</th>
              <th>Poin</th>
              <th>Kelas</th>
              <th>Mentor</th>
              <th>Tgl/Thn/Bln</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="pull-right pagin-area"></div>

  </div>

</div>

<script>
  // urls
  var urlByTgl = serviceURL + "f_mentoring_by_tgl.php?action=getMentoringByTglAll";
  var urlByBlnThn = serviceURL + "f_mentoring_by_my.php?action=getMentoringByMyAll";
  var urlDestroy = serviceURL + "f_mentoring_by_tgl.php?action=destroy";

  var str_url = document.URL;
  var url = new URL(str_url);
  var pos = url.searchParams.get('pos');
  var temp_type = 0;

  $(".btn-sw").click(function(event) {
    tableChange($(this));
  });

  $("#btn-destroy").click(function(event) {
    $("#modalDestroy").modal("show");

    $("#modalDestroy .destroy").click(function(event) {
      $.get(urlDestroy, function(data) {
        var dat = JSON.parse(data);
        if(dat == '1'){
          location.reload();
        }else{
          alert("Failed to destroy the data!");
        }
      });
    });
  });

  function tableChange(el){
    const tipe = el.data("tipe");

    if(tipe == 'tgl'){
      url.searchParams.set("pos", true);
      window.history.pushState("", "", url);
      temp_type = 2;
      $(".btn-sw:nth-child(2)").addClass('active');
      $(".btn-sw:nth-child(1)").removeClass('active');
    }else{
      url.searchParams.delete("pos");
      window.history.pushState("", "", url);
      temp_type = 1;
      $(".btn-sw:nth-child(1)").addClass('active');
      $(".btn-sw:nth-child(2)").removeClass('active');
    }

    $.get(((tipe == 'tgl') ? urlByTgl : (tipe == 'my') ? urlByBlnThn : ''),
      function(data, textStatus, xhr) {
        var dat = JSON.parse(data);
        dTable.clear();
        dTable.rows.add(dat['data']);
        dTable.rows().invalidate('data');
        dTable.columns.adjust().draw();
        dTable.draw(false);
    });
  }

  var dTable = $("#tblData").DataTable({
    ajax: {
      url: ((pos == null) ? urlByBlnThn : urlByTgl)
    },
    'dom': 'rtp',
		'pageLength': 10,
    columnDefs: [{
			targets: [ 0 ],
			searchable: false,
      orderable: false,
      data: null
		},
    {
			targets: [1],
      data: function(row, type, val, meta){
        return row.nama_kader
      }
		},
    {
      targets: [2],
      "data": function(row, type, val, meta){
        var aktifitas = '';
        pos = url.searchParams.get('pos');
        if(pos == null || temp_type == 1){
          aktifitas = row.count_checklist;
        }else if(pos != null || temp_type == 2){
          aktifitas = row.nama_materi;
        }
        return aktifitas;
      }
    },
    {
			targets: [3],
      data: function(row, type, val, meta){
        return row.sum_poin
      }
		},
    {
			targets: [4],
      data: function(row, type, val, meta){
        return row.nama_kelas
      }
		},
    {
			targets: [5],
      data: function(row, type, val, meta){
        return row.nama_mentor
      }
		},
    {
			targets: [6],
      "data": function(row, type, val, meta){
        var tgl = '';
        if(pos == null || temp_type == 1){
          tgl = row.bulan + "/" + row.tahun;
        }else if(pos != null || temp_type == 2){
          tgl = row.tgl;
        }
        return tgl;
      }
		}]
  });

  dTable.on('order.dt search.dt', function(){
		dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
	}).draw();

  $(".input-src").keyup(function(e) {
		dTable.search($(this).val()).draw();
	});

  $(".pagin-area").append($("#tblData_paginate"));


</script>

<!--
SUPER ADMINISTRATOR Can :
- CRUD superadministrator data - edit, delete, read, create [ALL DONE]
- CRUD admin data - edit, delete, read, create [ALL DONE]
- CRUD mentoring class - edit, read, create, delete [DONE]
- CRUD mentor - edit, read, create, delete [ALL DONE]
- CRUD kader
- view mentoring report
-->
