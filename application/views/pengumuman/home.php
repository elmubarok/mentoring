<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #ababab;
    width: 50px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
    vertical-align: middle;
  }
  .pagination{
    margin-top: 5px;
  }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]:after{ display:none; }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]{ padding-right: 0px;}
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">
    <div class="form-group">
      <button class="btn btn-info btn-fill" id="btnAdd">
        <i class="fa fa-plus"></i>&nbsp;Tambah
      </button>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px;">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px;">No</th>
              <th>Judul</th>
              <th>Konten</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="pull-right pagin-area"></div>

  </div>

</div>

<script>
  // urls
  var urlGetPengumuman = serviceURL + "f_pengumuman.php?action=getPengumumanAll";
  var urlGetPengumumanOne = serviceURL + "f_pengumuman.php?action=getPengumumanById";
  var urlUpdate = serviceURL + "f_pengumuman.php?action=updatePengumuman";
  var urlCreate = serviceURL + "f_pengumuman.php?action=create";
  var urlDel = serviceURL + "f_pengumuman.php?action=delete";

  jQuery(document).ready(function($) {
    initOptInsideModal();
  });

  var dTable = $("#tblData").DataTable({
    ajax: {
      url: urlGetPengumuman
    },
    'dom': 'rtp',
		'pageLength': 10,
    columnDefs: [{
			targets: [ 0, 3 ],
			searchable: false,
      orderable: false
		}],
    columns: [
      {"data": null},
      {"data": "title"},
      {
        render: function(data, type, row){
          return row.content.length > 50 ?
            row.content.substr( 0, 80 ) +'…' :
            row.content;
        }
      },
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.id+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id="'+row.id+'"><i class="fa fa-times"></i></div></div>'
        }
      }
    ]
  });

  dTable.on('order.dt search.dt', function(){
		dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
	}).draw();

  $(".input-src").keyup(function(e) {
		dTable.search($(this).val()).draw();
	});

  $(".pagin-area").append($("#tblData_paginate"));

  $("#btnAdd").click(function(event) {
    $("#modalPengumuman .modal-title").text("Tambah Data Baru");
    modalFlush();
    $("#modalPengumuman").modal('show');

    $("#modalPengumuman .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmPengumuman").serialize();
      createPengumuman(serialData);
      console.log(serialData);
    });
  });

  $(".table tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalPengumuman .modal-title").text("Edit");

    setEditedModal($(this));

    $("#modalPengumuman").modal('show');

    $("#modalPengumuman .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmPengumuman").serialize();
      updatePengumuman(serialData);
      console.log(serialData);
    });
  });
  $(".table tbody").on('click', '.btn-rem', function(event) {
    event.preventDefault();

    $("#modalUsrRem").modal('show');

    var el = $(this);

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModal(el);
    });
  });

  function pattern(which, data){
    var patternRes = "";

    switch(which){
      case 1:
        patternRes =
        $("<tr>")
          .append(
            $("<td>").html( // img
              $("<div>")
                .attr({
                  'class': 'img-container',
                  'style': 'background-image: url(<?=base_url('assets/img/no-photo.png')?>)'
                })
            ),
            $("<td>").text(data.nama_lengkap), // nama
            $("<td>").text(data.hp), // hp
            $("<td>").text(data.email), // email
            $("<td>").text(data.jkelamin), // jenis kelamin
            $("<td>")
              .attr({'style': 'text-align: right'})
              .html(
                $("<div>")
                  .attr({'class': 'btn-group'})
                  .append(
                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                        'title': 'Edit',
                        'data-id': data.id,
                        'data-wajihah': data.wajihah_id,
                        'data-fakultas': data.fakultas_id,
                        'data-jurusan': data.jurusan_id,
                        'data-jabatan': data.jabatan_id,
                        'data-level': data.level,
                        'data-kelamin': data.jkelamin
                      })
                      .html($("<i>").attr({'class': 'fa fa-edit'})),

                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                        'title': 'Remove',
                        'data-id': data.id
                      })
                      .html($("<i>").attr({'class': 'fa fa-times'}))
                  ) // append
              )
        );
      break;
      case 2:
        patternRes =
        $("<option>")
          .attr({
            'value': data.id
          })
          .text(data.nama);
      break;
    }
    return patternRes;
  }

  function initOptInsideModal(){
    /*ldkList
      jurusanList
      fakultasList
      jabatanList
      levelList*/

    $("#ldkList, #jurusanList, #fakultasList, #jabatanList, #levelList, #nim, #angkatan")
      .parents(".form-group")
      .remove();
  }

  function setEditedModal(el){
    var id = el.data('id');

    $.get(urlGetPengumumanOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;

      $("#inpIdP").val(id);
      $("#judulP").val(edit.title);
      $("#kontenP").val(edit.content);

      console.log(edit);
    });
  }

  function setDelModal(el){
    var id = el.data('id');

    $.get(urlDel, {id:id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }

  function updatePengumuman(data){
    $.post(urlUpdate, data + "&level=3", function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function createPengumuman(data){
    $.post(urlCreate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function modalFlush(){
    $("#modalPengumuman input, #modalPengumuman textarea").val('');
  }
</script>

<!--
SUPER ADMINISTRATOR Can :
- CRUD superadministrator data - edit, delete, read, create [ALL DONE]
- CRUD admin data - edit, delete, read, create [ALL DONE]
- CRUD mentoring class - edit, read, create, delete [DONE]
- CRUD mentor - edit, read, create, delete [ALL DONE]
- CRUD kader
- view mentoring report
-->
