<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #ababab;
    width: 50px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
  }
  .pagination{
    margin-top: 5px;
  }
  .card-footer{
    clear: both;
  }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]:after{ display:none; }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]{ padding-right: 0px;}
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">
    <div class="form-group">
      <button class="btn btn-info btn-fill" id="btnAdd">
        <i class="fa fa-plus"></i>&nbsp;Tambah
      </button>
      <button class="btn btn-primary btn-outline" id="btnFill">
        <i class="fa fa-user-plus"></i>&nbsp;Isi Kelas
      </button>
      <button class="btn btn-success pull-right" id="btnUpdateClass">
        <i class="fa fa-refresh"></i>&nbsp;Perbarui Kelas
      </button>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px;">No</th>
              <th>Nama Kelas</th>
              <th>Hari</th>
              <th>Jam</th>
              <th>Tempat</th>
              <th>Jenis Kelas</th>
              <th>Mentor</th>
              <th></th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
    <div class="pull-right pagin-area"></div>

  </div>

  <div class="container-fluid">
    <div class="row">
      <!-- Laki2 -->
      <div class="col-md-6">

        <div class="row">
          <h4 class="col-md-12">Kelas Laki-laki</h4>
          <div class="form-group col-md-6">
            <select class="form-control input-sm selectLaki">
              <option disabled selected>Pilih Kelas</option>
              <option value="">Semua Kelas</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <input type="text" class="input-sm form-control input-srcL"
              placeholder="Cari nama kader ...">
          </div>
        </div>

        <div class="card" style="margin-bottom: 0px">
          <div class="card-body">
            <table class="table table-hover table-striped table-with-thumb" id="tblDataL" style="width: 100%;">
              <thead>
                <tr>
                  <th style="width: 10px;">No</th>
                  <th>Nama Kelas</th>
                  <th>Nama Kader</th>
                  <th></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <div class="card-footer">
            <div class="pull-right pagin-areaL"></div>
          </div>
        </div>
      </div>

      <!-- Perempuan -->
      <div class="col-md-6">

        <div class="row">
          <h4 class="col-md-12">Kelas Perempuan</h4>
          <div class="form-group col-md-6">
            <select class="form-control input-sm selectPerempuan">
              <option disabled selected>Pilih Kelas</option>
              <option value="">Semua Kelas</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <input type="text" class="input-sm form-control input-srcP"
              placeholder="Cari nama kader ...">
          </div>
        </div>

        <div class="card" style="margin-bottom: 0px">
          <div class="card-body">
            <table class="table table-hover table-striped table-with-thumb" id="tblDataP" style="width: 100%;">
              <thead>
                <tr>
                  <th style="width: 10px;">No</th>
                  <th>Nama Kelas</th>
                  <th>Nama Kader</th>
                  <th></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <div class="card-footer">
            <div class="pull-right pagin-areaP"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<script>
  // urls
  var urlGetKelasAll = serviceURL + "f_kelas.php?action=getKelasAll";
  var urlGetKelasOne = serviceURL + "f_kelas.php?action=getKelasById";
  var urlGetMentorAll = serviceURL + "f_kelas.php?action=getMentorAll";
  var urlUpdate = serviceURL + "f_kelas.php?action=updateKelas";
  var urlCreate = serviceURL + "f_kelas.php?action=create";
  var urlDel = serviceURL + "f_kelas.php?action=delete";

  var urlDelType = serviceURL + "f_grouping.php?action=delete";
  var urlUpdateType = serviceURL + "f_grouping.php?action=updateGrouping";
  var urlGetKelasType = serviceURL + "f_grouping.php?action=getKelasType";
  var urlGetGroupingL = serviceURL + "f_grouping.php?action=getGroupingByType&type=l";
  var urlGetGroupingP = serviceURL + "f_grouping.php?action=getGroupingByType&type=p";
  var urlIsiKelas = serviceURL + "f_grouping.php?action=isiKelas"
  var urlGetGroupingOne = serviceURL + "f_grouping.php?action=getGroupingById";
  var urlRenew = serviceURL + "f_grouping.php?action=renew";
  var urlCheckNew = serviceURL + "f_grouping.php?action=checknew";

  initOptInsideModal(1);

  $("#btnFill").click(function(event) {
    $.get(urlIsiKelas,
      function(data, textStatus, xhr) {
        if(data == 'Success'){
          location.reload();
        }else{
          alert("Something wrong");
        }
    });
  });
  $("#btnAdd").click(function(event) {
    $("#modalKelas .modal-title").text("Tambah Data Baru");
    modalFlush();
    $("#modalKelas").modal('show');

    $("#modalKelas .modal-footer").on('click', '#btnKelasSave', function(event) {
      $(this).button({loadingText: 'Sedang proses ...'});
      $(this).button("loading");

      var serialData = $("#frmKelasData").serialize();
      createKelas(serialData);
      console.log(serialData);
    });
  });

  // update kelas
  $("#btnUpdateClass").click(function(event) {
    $(this).button({loadingText: 'Sedang proses ...'});
    var btn = $(this).button("loading");
    $.get(urlRenew, function(data) {
      if(data == 'done'){
        btn.button("reset");
        location.reload();
      }
    });
  });
  // enable perbarui kelas
  // <span class="label label-danger">2</span>
  $.get(urlCheckNew, function(data) {
    if(data > 0){
      $("#btnUpdateClass").removeAttr('disabled');
      $("#btnUpdateClass").append('<span class="label label-danger">' + data + '</span>');
    }else{
      $("#btnUpdateClass").attr('disabled', 'disabled');
    }
  });

  $(".table#tblData tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalKelas .modal-title").text("Edit");

    $("#mentorList").val($(this).data('mentor'));

    setEditedModal($(this));

    $("#modalKelas").modal('show');

    $("#modalKelas .modal-footer").on('click', '#btnKelasSave', function(event) {
      $(this).button({loadingText: 'Sedang proses ...'});
      $(this).button("loading");

      var serialData = $("#frmKelasData").serialize();
      updateUser(serialData);
      console.log(serialData);
    });
  });
  $(".table#tblData tbody").on('click', '.btn-rem', function(event) {
    event.preventDefault();

    var el = $(this);
    $("#modalUsrRem").modal('show');

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModal(el);
    });
  });

  $(".table#tblDataL tbody, .table#tblDataP tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalGrouping #kaderNama").text($(this).data("nama"));

    setEditedModalType($(this), $(this).data("jkelamin"));

    $("#modalGrouping").modal('show');

    $("#modalGrouping .modal-footer").on('click', '#btnKelasSave', function(event) {
      var serialData = $("#frmGroupingData").serialize();
      updateUserKelas(serialData);
      console.log(serialData);
    });
  });
  $(".table#tblDataL tbody, .table#tblDataP tbody").on('click', '.btn-rem', function(event) {
    var el = $(this);
    $("#modalUsrRem").modal('show');

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModalType(el);
    });
  });

  function pattern(which, data){
    var patternRes = "";

    switch(which){
      case 1:
        patternRes =
        $("<tr>")
          .append(
            $("<td>").text(data.nama_kelas), // nim
            $("<td>").text(data.hari), // nama
            $("<td>").text(data.waktu), // ft
            $("<td>").text(data.tempat), // jurusan
            $("<td>").text(data.link_group_wa), // wajihah
            $("<td>").text(data.nama_mentor), // jabatan
            $("<td>")
              .attr({'style': 'text-align: right'})
              .html(
                $("<div>")
                  .attr({'class': 'btn-group'})
                  .append(
                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                        'title': 'Edit',
                        'data-id': data.id,
                        'data-mentor': data.mentor_id
                      })
                      .html($("<i>").attr({'class': 'fa fa-edit'})),

                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                        'title': 'Remove',
                        'data-id': data.id
                      })
                      .html($("<i>").attr({'class': 'fa fa-times'}))
                  ) // append
              )
        );
      break;
      case 2:
        patternRes =
        $("<option>")
          .attr({
            'value': data.id
          })
          .text(data.nama_lengkap);
      break;
      case 3:
        patternRes =
          $("<div>")
            .attr({'class': 'btn-group'})
            .append(
              $("<div>")
                .attr({
                  'type': 'button',
                  'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                  'title': 'Edit',
                  'data-id': "data.grouping_id",
                  'data-kelas': "data.kelas_id",
                  'data-kader': "data.kader_id"
                })
                .html($("<i>").attr({'class': 'fa fa-edit'})),

              $("<div>")
                .attr({
                  'type': 'button',
                  'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                  'title': 'Remove',
                  'data-id': "data.id"
                })
                .html($("<i>").attr({'class': 'fa fa-times'}))
            ); // append
      break;
    }
    return patternRes;
  }

  function initOptInsideModal(which){
    /*ldkList
      jurusanList
      fakultasList
      jabatanList
      levelList*/

    switch(which){
      case 1:
        $.get(urlGetMentorAll, function(data, textStatus, xhr) {
          var mentor = JSON.parse(data).items;
          for(var i=0; i<mentor.length; i++){
            $("#mentorList").append(pattern(2, mentor[i]))
          }
        });
      break;
    }
  }

  function setEditedModal(el){
    var id = el.data('id');

    $.get(urlGetKelasOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;

      $("#inpIdKelas").val(edit.id);
      $("#inpKelasName").val(edit.nama_kelas);
      $("#inpLokasi").val(edit.tempat);
      $("#inpHari").val(edit.hari);
      $("#inpJam").val(edit.waktu);
      $("#inpLinkWa").val(edit.link_group_wa);
      $("#jenisKelas").val(edit.jkelamin);

      console.log(edit);
    });

    $("#modalKelas .modal-footer #btnKelasSave").button("reset");
  }

  function setEditedModalType(el, tipe){
    var id = el.data('id');
    $.get(urlGetGroupingOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;
      $("#inpIdGroup").val(id);
      $("#inpIdGroupKader").val(el.data("id_kader"));
      $.get(urlGetKelasType,
        {type: tipe}, function(dataaa, textStatus, xhr) {
          var datk = JSON.parse(dataaa).items;
          console.log(datk);
          $("#modalGrouping #kelasList").html(
            $("<option>").attr({
              "disabled": "disabled",
              "selected": "selected"
            }).html("Pilih Kelas")
          );
          for(var i=0; i<datk.length; i++){
            $("#modalGrouping #kelasList").append(
              $("<option>").attr('value', datk[i].id).text(datk[i].nama_kelas)
            )
          }
      });
      console.log(edit);
    });
  }

  function setDelModal(el){
    var id = el.data('id');

    $.get(urlDel, {id:id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }
  function setDelModalType(el){
    var id = el.data('id');

    $.get(urlDelType, {id:id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }

  function updateUser(data){
    $.post(urlUpdate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }
  function updateUserKelas(data){
    $.post(urlUpdateType, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function createKelas(data){
    $.post(urlCreate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
      $("#modalKelas .modal-footer #btnKelasSave").button("reset");
    });
  }

  function modalFlush(){
    $("#modalKelas input").val('');
    $("#modalKelas select").val('null');
  }
</script>
<?php $this->load->view('kelas/table_kelas');
      $this->load->view('kelas/table_laki');
      $this->load->view('kelas/table_perempuan');?>
