<script>
  var dTable = $("#tblData").DataTable({
    ajax: {
      url: urlGetKelasAll
    },
    'dom': 'rtp',
    'pageLength': 10,
    columnDefs: [{
      targets: [ 0, 7 ],
      searchable: false,
      orderable: false
    }],
    columns: [
      {"data": null},
      {"data": "nama_kelas"},
      {"data": "hari"},
      {"data": "waktu"},
      {"data": "tempat"},
      {"data": "jkelamin"},
      {"data": "nama_mentor"},
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.id+'" data-mentor="'+row.mentor_id+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id="'+row.id+'"><i class="fa fa-times"></i></div></div>'
        }
      }
    ]
  });

  dTable.on('order.dt search.dt', function(){
    dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
  }).draw();

  $(".input-src").keyup(function(e) {
    dTable.search($(this).val()).draw();
  });

  $(".pagin-area").append($("#tblData_paginate"));

</script>
