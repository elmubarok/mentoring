<script>
  var dTableL = $("#tblDataL").DataTable({
    ajax: {
      url: urlGetGroupingL
    },
    'dom': 'rtp',
    'pageLength': 10,
    columnDefs: [{
      targets: [ 0, 3 ],
      searchable: false,
      orderable: false
    }],
    columns: [
      {"data": null},
      {"data": "nama_kelas"},
      {"data": "nama_kader"},
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.grouping_id+'" data-id_kader="'+row.kader_id+'" data-nama="'+row.nama_kader+'" data-jkelamin="'+row.jkelamin+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id="'+row.grouping_id+'"><i class="fa fa-times"></i></div></div>'
        }
      }
    ],
    initComplete: function(){
      this.api().column(1).every(function() {
        var col = this;
        var select = $(".selectLaki").on('change', function(e) {
          var val = $.fn.dataTable.util.escapeRegex(
            $(this).val()
          );
          col
            .search(val ? '^'+val+'$' : '', true, false)
            .draw();
        });

        col.data().unique().sort().each(function(d,j){
          console.log(j);
          select.append('<option value="'+d+'">' + d + '</option>')
        });
      })
    }
  });

  dTableL.on('order.dt search.dt', function(){
    dTableL.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
  }).draw();

  $(".input-srcL").keyup(function(e) {
    dTableL.column(2).search($(this).val()).draw();
  });

  $(".pagin-areaL").append($("#tblDataL_paginate"));

</script>
