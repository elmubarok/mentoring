<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #ababab;
    width: 50px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
    vertical-align: middle;
  }
  .pagination{
    margin-top: 5px;
  }
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">
    <div class="form-group">
      <button class="btn btn-info btn-fill" id="btnAdd">
        <i class="fa fa-plus"></i>&nbsp;Tambah
      </button>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px;">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px;"></th>
              <th style="width:50px;"></th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Fakultas</th>
              <th>Jurusan</th>
              <th>Wajihah</th>
              <th>Jabatan</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="pull-right pagin-area"></div>

  </div>

</div>

<script>
  // urls
  var urlGetSAdmin = serviceURL + "f_admin.php?action=getSUserAll";
  var urlGetSAdminOne = serviceURL + "f_admin.php?action=getSUserById";
  var urlGetLdk = serviceURL + "f_admin.php?action=getLdkAll";
  var urlGetJur = serviceURL + "f_admin.php?action=getJurusanAll";
  var urlGetFak = serviceURL + "f_admin.php?action=getFakultasAll";
  var urlGetJab = serviceURL + "f_admin.php?action=getJabatanAll";
  var urlGetLev = serviceURL + "f_admin.php?action=getLevelAll";
  var urlUpdate = serviceURL + "f_admin.php?action=updateUser";
  var urlCreate = serviceURL + "f_admin.php?action=create";
  var urlDel = serviceURL + "f_admin.php?action=delete";
  var usrUrl = "http://localhost/Mentoring/assets/img/";
  var urlImg = serviceURL + "uploadImage.php";

  initOptInsideModal(1);
  initOptInsideModal(2);
  initOptInsideModal(3);
  initOptInsideModal(4);
  initOptInsideModal(5);

  jQuery(document).ready(function($) {
    $("#btnFileAvatar").click(function(event) {
      $("#inputAvatar").trigger('click');
    });

    $("#inputAvatar").change(function() {
      processAvatars(this);
    });
  });

  function processAvatars(el){
    if (el.files && el.files[0]) {
      var reader = new FileReader;
      reader.onload = function(e) {
        $('#imgAvatarInner').attr('src', e.target.result);
        var resize = new Croppie($('#imgAvatarInner')[0], {
          viewport: { width: 128, height: 128 },
          boundary: { width: 150, height: 150 },
          showZoomer: false,
          enableResize: false,
          enableOrientation: true
        });
        $('#btnAvatarGroup').fadeIn("fast", function(){
          $("#btnAvatarGroup").on('click', '#btnUpAvatarCls', function(event) {
            removeCroppie(resize);
          });
          $("#btnAvatarGroup").on('click', '#btnUpAvatarUp', function(event) {
            resize.result('base64').then(function(dataImg) {
                var data = [{ image: dataImg }, { name: 'myimgage.jpg' }];
                // use ajax to send data to php
                $('#imgAvatar').css('background-image', 'url(' + dataImg + ')');
                $("#inpAvatarFinal").val(dataImg);
                removeCroppie(resize);
              })
          });
        });
      }
      reader.readAsDataURL(el.files[0]);
    }else{
      alert("PUt");
    }
  }
  function removeCroppie(r){
    $('#imgAvatarInner').attr('src', '');
    $("#inputAvatar").val('');
    $('#btnAvatarGroup').fadeOut("fast");
    $(".croppie-container").remove();
    $("#imgAvatarInnerContainer").html('<img src="" id="imgAvatarInner">');
    r = null;
  }

  var dTable = $("#tblData").DataTable({
    ajax: {
      url: urlGetSAdmin
    },
    'dom': 'rtp',
		'pageLength': 10,
    columnDefs: [{
			targets: [ 0, 1, 8 ],
			searchable: false,
      orderable: false
		}],
    columns: [
      {"data": null},
      {
        render: function(data, type, row){
          return '<div class="img-container" style="background-image: url(http://localhost/Mentoring/assets/img/'+row.username+'.jpg'+')"></div>'
        }
      },
      {"data": "nim"},
      {"data": "nama_lengkap"},
      {"data": "fakultas"},
      {"data": "jurusan"},
      {"data": "wajihah"},
      {"data": "jabatan"},
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.id+'" data-wajihah="'+row.wajihah_id+'" data-fakultas="'+row.fakultas_id+'" data-jurusan="'+row.jurusan_id+'" data-jabatan="'+row.jabatan_id+'" data-level="'+row.level+'" data-kelamin="'+row.jkelamin+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id='+row.id+'><i class="fa fa-times"></i></div></div>'
        }
      }
    ]
  });

  dTable.on('order.dt search.dt', function(){
		dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
	}).draw();

  $(".input-src").keyup(function(e) {
		dTable.search($(this).val()).draw();
	});

  $(".pagin-area").append($("#tblData_paginate"));

  $("#btnAdd").click(function(event) {
    $("#modalUsrEdit .modal-title").text("Tambah Data Baru");
    modalFlush();
    $("#modalUsrEdit").modal('show');

    $("#uname").keyup(function(event) {
      $("#avatarName").val($(this).val());
    });
    $("#modalUsrEdit .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmAdminData").serialize();
      createUser(serialData);
      console.log(serialData);
    });
  });

  $(".table tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalUsrEdit .modal-title").text("Edit");

    $("#ldkList").val($(this).data('wajihah'));
    $("#jurusanList").val($(this).data('jurusan'));
    $("#fakultasList").val($(this).data('fakultas'));
    $("#jabatanList").val($(this).data('jabatan'));
    $("#levelList").val($(this).data('level'));
    $("#jkList").val($(this).data('kelamin'));

    setEditedModal($(this));

    $("#modalUsrEdit").modal('show');

    $("#modalUsrEdit .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmAdminData").serialize();
      updateUser(serialData);
      console.log(serialData);
    });
  });
  $(".table tbody").on('click', '.btn-rem', function(event) {
    event.preventDefault();

    $("#modalUsrRem").modal('show');
    var el = $(this);

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModal(el);
    });
  });

  function pattern(which, data){
    var patternRes = "";

    switch(which){
      case 1:
        patternRes =
        $("<tr>")
          .append(
            $("<td>").html( // img
              $("<div>")
                .attr({
                  'class': 'img-container',
                  'style': 'background-image: url(<?=base_url('assets/img/no-photo.png')?>)'
                })
            ),
            $("<td>").text(data.nim), // nim
            $("<td>").text(data.nama_lengkap), // nama
            $("<td>").text(data.fakultas), // ft
            $("<td>").text(data.jurusan), // jurusan
            $("<td>").text(data.wajihah), // wajihah
            $("<td>").text(data.jabatan), // jabatan
            $("<td>")
              .attr({'style': 'text-align: right'})
              .html(
                $("<div>")
                  .attr({'class': 'btn-group'})
                  .append(
                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                        'title': 'Edit',
                        'data-id': data.id,
                        'data-wajihah': data.wajihah_id,
                        'data-fakultas': data.fakultas_id,
                        'data-jurusan': data.jurusan_id,
                        'data-jabatan': data.jabatan_id,
                        'data-level': data.level,
                        'data-kelamin': data.jkelamin
                      })
                      .html($("<i>").attr({'class': 'fa fa-edit'})),

                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                        'title': 'Remove',
                        'data-id': data.id
                      })
                      .html($("<i>").attr({'class': 'fa fa-times'}))
                  ) // append
              )
        );
      break;
      case 2:
        patternRes =
        $("<option>")
          .attr({
            'value': data.id
          })
          .text(data.nama);
      break;
    }
    return patternRes;
  }

  function initOptInsideModal(which){
    /*ldkList
      jurusanList
      fakultasList
      jabatanList
      levelList*/

    switch(which){
      case 1:
        $.get(urlGetLdk, function(data, textStatus, xhr) {
          var ldk = JSON.parse(data).items;
          for(var i=0; i<ldk.length; i++){
            $("#ldkList").append(pattern(2, ldk[i]))
          }
        });
      break;
      case 2:
        $.get(urlGetJur, function(data, textStatus, xhr) {
          var jur = JSON.parse(data).items;
          for(var i=0; i<jur.length; i++){
            $("#jurusanList").append(pattern(2, jur[i]))
          }
        });
      break;
      case 3:
        $.get(urlGetFak, function(data, textStatus, xhr) {
          var fak = JSON.parse(data).items;
          for(var i=0; i<fak.length; i++){
            $("#fakultasList").append(pattern(2, fak[i]))
          }
        });
      break;
      case 4:
        $.get(urlGetJab, function(data, textStatus, xhr) {
          var jab = JSON.parse(data).items;
          for(var i=0; i<jab.length; i++){
            $("#jabatanList").append(pattern(2, jab[i]))
          }
        });
      break;
      case 5:
        $.get(urlGetLev, function(data, textStatus, xhr) {
          var lev = JSON.parse(data).items;
          for(var i=0; i<lev.length; i++){
            $("#levelList").append(pattern(2, lev[i]))
          }
        });
      break;
    }
  }

  function setEditedModal(el){
    var id = el.data('id');

    $.get(urlGetSAdminOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;

      $("#inpId").val(edit.id);
      $('#imgAvatar').css('background-image', 'url(' + usrUrl + edit.username + '.jpg' + ')');
      $("#avatarName").val(edit.username);
      $("#callname").val(edit.nama_panggilan);
      $("#fullname").val(edit.nama_lengkap);
      $("#addrown").val(edit.alamat_asal);
      $("#addrdom").val(edit.alamat_domisili);
      $("#hp").val(edit.hp);
      $("#email").val(edit.email);
      $("#nim").val(edit.nim);
      $("#angkatan").val(edit.angkatan);
      $("#uname").val(edit.username);
      $("#uname").keyup(function(event) {
        $("#avatarName").val($(this).val());
      });
      $("#upswd").val(edit.password);

      console.log(edit);
    });
  }

  function setDelModal(el){
    var id = el.data('id');

    $.get(urlDel, {id: id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }

  function updateUser(datas){
    $.post(urlImg, datas, function(data, textStatus, xhr) {
      $.post(urlUpdate, datas, function(data, textStatus, xhr) {
        if(data == 'Success'){
          location.reload();
        }
      });
    });
  }

  function createUser(data){
    $.post(urlCreate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function modalFlush(){
    $("#modalUsrEdit input").val('');
    $("#modalUsrEdit select").val('null');
    $("#levelList").val('1');
    $('#imgAvatar').css('background-image', 'url(' + usrUrl + 'no-photo.png' + ')');
  }
</script>

<!--
SUPER ADMINISTRATOR Can :
- CRUD superadministrator data - edit, delete, read, create [ALL DONE]
- CRUD admin data - edit, delete, read, create [ALL DONE]
- CRUD mentoring class - edit, read, create, delete [ALL DONE]
- CRUD mentor - edit, read, create, delete [ALL DONE]
- CRUD kader - - edit, read, create, delete [ALL DONE]
- view mentoring report
-->
