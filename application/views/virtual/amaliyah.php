<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Amaliyah</title>
    <script
      src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js"
      type="text/javascript"></script>
    <style media="screen">
      .form{
        display: block;
        padding: 2px 6px;
        text-transform: capitalize;
      }
      .forms1, .forms2{
        margin-bottom: 10px;
      }
      button{
        margin: 12px 6px;
      }
    </style>
  </head>
  <body>
    <?php
      $arrWajib = array(
        "subuh" => 44,
        "dhuhur" => 45,
        "asar" => 46,
        "maghrib" => 47,
        "isya" => 48
      );
      $arrSunnah = array(
        "qobliyah subuh" => 43,
        "qobliyah dhuhur" => 50,
        "badiyah maghrib" => 51,
        "badiyah isya" => 52,
        "malam & witir" => 42,
        "dhuha" => 49,
        "syuruq" => 53
      );
      $arrOther = array(
        "dzikir pagi" => 54,
        "puasa sunnah" => 55,
        "dzikir petang" => 56,
        "membaca al-quran" => 57,
        "sedekah/infaq" => 58,
        "membaca buku islami" => 59,
        "olahraga" => 60
      );
    ?>

    <div class="btns">
      <button onclick="window.open('<?=site_url('VirtualMobile/aktifitas')?>', '_self')">Aktifitas Saya</button>
      <button onclick="window.open('<?=site_url('VirtualMobile')?>', '_self')">Amaliyah</button>
      <button onclick="window.open('<?=site_url('VirtualMobile/quran')?>', '_self')">Hafalan Quran</button>
      <button onclick="window.open('<?=site_url('VirtualMobile/hadits')?>', '_self')">Hafalan Hadits</button>
    </div>

    <h1>Amaliyah</h1>

    <form class="forms1">
      <?php foreach($arrWajib as $k => $v){ ?>
      <div class="form">
        <input id="<?=$k?>" type="checkbox" name="amal[]" value="<?=$v?>">
        <label for="<?=$k?>"><?=$k?></label>
      </div>
      <?php } ?>
    </form>
    <form class="forms2">
      <?php foreach($arrSunnah as $k => $v){ ?>
      <div class="form">
        <input id="<?=$k?>" type="checkbox" name="amal[]" value="<?=$v?>">
        <label for="<?=$k?>"><?=$k?></label>
      </div>
      <?php } ?>
    </form>
    <form class="forms3">
      <?php foreach($arrOther as $k => $v){ ?>
      <div class="form">
        <input id="<?=$k?>" type="checkbox" name="amal[]" value="<?=$v?>">
        <label for="<?=$k?>"><?=$k?></label>
      </div>
      <?php } ?>
    </form>

    <button id="btnSave">Simpan</button>

    <script>
      var arrSholatWajib = [
        "subuh", "dhuhur", "asar", "maghrib", "isya"
      ];
      var arrSholatSunnah = [
        "qobliyah subuh", "qobliyah dhuhur", "badiyah maghrib", "badiyah isya",
        "malam & witir", "dhuha", "syuruq"
      ];
      var arrOther = [
        "dzikir pagi", "puasa sunnah", "dzikir petang", "membaca al-quran",
        "sedekah/infaq", "membaca buku islami", "olahraga"
      ];
      var urlCreate = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=insert';

      $("#btnSave").click(function(event) {
        var wajib = $(".forms1").serialize();
        var sunnah = $(".forms2").serialize();
        var other = $(".forms3").serialize();
        var kader = "&kader_id=4";
        var which = "&which=amal";

        $.post(urlCreate, wajib+"&"+sunnah+"&"+other+kader+which, function(data, textStatus, xhr) {
          var dat = JSON.parse(data)[0];
          if(dat.stat){
            location.reload();
          }else{
            alert("Error: look at network tools");
          }
        });
      });

      function pattern(idx, data, caption){
        var patternRes =
          $("<div>")
            .attr({
              'class': 'form'
            })
            .append(
              $("<input>")
                .attr({
                  'type': 'checkbox',
                  'name': data.split(" ").join("_"),
                  'id': data.split(" ").join("_"),
                  'value': 1
                }),
              $("<label>")
                .attr({
                  'for': data.split(" ").join("_")
                })
                .text(caption)
            );
          return patternRes;
      }
    </script>

  </body>
</html>
