<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Aktifitas</title>
    <script
      src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js"
      type="text/javascript"></script>
    <style media="screen">
      body.active{
        overflow: hidden;
      }
      button{
        margin: 12px 6px;
      }
      div{
        margin-bottom: 16px;
      }
      table, th, td{
        border: 1px solid #333;
        border-collapse: collapse;
        padding: 6px;
      }
      .table2{
        position: fixed;
        width: 100%; height: 100%;
        left: 0; top: 0;
        background: #fff;
        visibility: hidden;
        overflow: auto;
      }
      .table2 div{
        padding: 8px;
      }
      .table2.active{
        visibility: visible;
      }
    </style>
  </head>
  <body>
    <div class="btns">
      <button onclick="window.open('<?=site_url('VirtualMobile/aktifitas')?>', '_self')">Aktifitas Saya</button>
      <button onclick="window.open('<?=site_url('VirtualMobile')?>', '_self')">Amaliyah</button>
      <button onclick="window.open('<?=site_url('VirtualMobile/quran')?>', '_self')">Hafalan Quran</button>
      <button onclick="window.open('<?=site_url('VirtualMobile/hadits')?>', '_self')">Hafalan Hadits</button>
    </div>

    <h1>Aktifitas Saya</h1>

    <div>
      <select name="tgl_rekap" id="tgl_rekap">
        <option selected value="all">Show All</option>
      </select>
    </div>
    <div class="table1">
      <table>
        <thead>
          <th>No</th>
          <th>Tanggal</th>
          <th>Tipe</th>
          <th>Total Poin</th>
          <th></th>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

    <div class="table2">
      <div>
        <button id="tutup">Tutup</button>
        <h1>Detail 2019-05-08</h1>
        <table>
          <thead>
            <th>No</th>
            <th>Nama Materi</th>
            <th>Poin</th>
            <th>Tipe</th>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>

    <script>
      var urlGetAktifitas = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=rekap_tgl';
      var urlGetAktifitasOne = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=rekap_tgl_one';
      var urlGetAktifitasTgls = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=get_aktifitas_tgl';
      var urlGetAktifitasMateri = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=get_aktifitas_materi';

      $.get(urlGetAktifitas, {kader_id: '4'}, function(data, textStatus, xhr) {
        var dat = JSON.parse(data);
        var act = dat[1].data;
        console.log(act[0]);

        for(var i=0; i<act.length; i++){
          $(".table1 tbody").append(
            pattern(1, (i+1), act[i])
          );
        }
      });

      $.get(urlGetAktifitasTgls, {kader_id: '4'}, function(data, textStatus, xhr) {
        var dat = JSON.parse(data);
        var tgl = dat[1].data;
        console.log(tgl[0]);

        for(var i=0; i<tgl.length; i++){
          $("#tgl_rekap").append(
            pattern(3, (i+1), tgl[i])
          );
        }
      });

      $("#tgl_rekap").change(function(event) {
        if($(this).val() == "all"){
          $(".table1 table tbody tr").remove();
          $.get(urlGetAktifitas, {kader_id: '4'}, function(data, textStatus, xhr) {
            var dat = JSON.parse(data);
            var act = dat[1].data;
            console.log(act[0]);

            for(var i=0; i<act.length; i++){
              $(".table1 tbody").append(
                pattern(1, (i+1), act[i])
              );
            }
          });
        }else{
          $(".table1 table tbody tr").remove();
          $.get(urlGetAktifitasOne,
            {tgl:$(this).val(),
            kader_id: '4'}, function(data, textStatus, xhr) {
            var dat = JSON.parse(data);
            var act = dat[1].data;
            console.log(act[0]);

            for(var i=0; i<act.length; i++){
              $(".table1 tbody").append(
                pattern(1, (i+1), act[i])
              );
            }
          });
        }
        $("#tgl_rekap").val($(this).val());
      });

      $("#tutup").click(function(event) {
        $(".table2").removeClass('active');
      });

      $(".table1 table tbody").on('click', 'a', function(event) {
        event.preventDefault();
        $(".table2 table tbody tr").remove();
        $.get(urlGetAktifitasMateri,
          {tgl:$(this).data('tgl'),
          tipe:$(this).data('tipe'),
          kader_id: '4'},
          function(data, textStatus, xhr) {
          var dat = JSON.parse(data);
          var act = dat[1].data;
          console.log(act[0]);

          for(var i=0; i<act.length; i++){
            $(".table2 tbody").append(
              pattern(4, (i+1), act[i])
            );
          }
        });

        $(".table2").addClass('active');
      });

      function pattern(mode, idx, data){
        switch (mode) {
          case 1:
            var patternRes =
              $("<tr>").
                append(
                  $("<td>").text(idx),
                  $("<td>").text(data.tgl),
                  $("<td>").text(
                    (data.is_amal != 0) ? "Amaliyah" :
                    (data.is_hadist != 0) ? "Hafalan Hadist" :
                    (data.is_surat != 0) ? "Hafalan Surat" : ""
                  ),
                  $("<td>").text(data.rekap_poin),
                  $("<td>").append(
                    $("<a>")
                      .text("Detail")
                      .attr({
                        'href': 'javascript:void(0)',
                        'data-tgl': data.tgl,
                        'data-tipe':
                          (data.is_amal != 0) ? "is_amal" :
                          (data.is_hadist != 0) ? "is_hadist" :
                          (data.is_surat != 0) ? "is_surat" : ""})
                  )
                );
          break;
          case 2:
            var patternRes =
              $("<tr>").
                append(
                  $("<td>").text(idx),
                  $("<td>").text(data.tgl),
                  $("<td>").text(
                    (data.is_amal != 0) ? "Amaliyah" :
                    (data.is_hadist != 0) ? "Hafalan Hadist" :
                    (data.is_surat != 0) ? "Hafalan Surat" : ""
                  ),
                  $("<td>").text(data.rekap_poin),
                  $("<td>").append(
                    $("<a>")
                      .text("Detail")
                      .attr({'href': ''})
                  )
                );
          break;
          case 3:
            var patternRes =
              $("<option>")
                .attr({
                  'value': data.tgl
                })
                .text(data.tgl);
          break;
          case 4:
            var patternRes =
              $("<tr>").
                append(
                  $("<td>").text(idx),
                  $("<td>").text(data.nama_materi),
                  $("<td>").text(data.rekap_poin),
                  $("<td>").text(
                    (data.is_amal != 0) ? "Amaliyah" :
                    (data.is_hadist != 0) ? "Hafalan Hadist" :
                    (data.is_surat != 0) ? "Hafalan Surat" : ""
                  )
                );
          break;
        }
        return patternRes;
      }
    </script>

  </body>
</html>
