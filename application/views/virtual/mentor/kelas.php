<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Aktifitas</title>
    <script
      src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js"
      type="text/javascript"></script>
    <style media="screen">
      body.active{
        overflow: hidden;
      }
      button{
        margin: 12px 6px;
      }
      div{
        margin-bottom: 16px;
      }
      table, th, td{
        border: 1px solid #333;
        border-collapse: collapse;
        padding: 4px 12px;
      }
      .table2{
        position: fixed;
        width: 100%; height: 100%;
        left: 0; top: 0;
        background: #fff;
        visibility: hidden;
        overflow: auto;
      }
      .table2 div{
        padding: 8px;
      }
      .table2.active{
        visibility: visible;
      }
    </style>
  </head>
  <body>

    <h1>Kelas Mentoring Saya</h1>

    <div class="table1">
      <table>
        <thead>
          <th>No</th>
          <th>Kelas</th>
          <th></th>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

    <script>
      var urlGet =
        'http://localhost/Mentoring/service/mobile/latest/mentor.php';

      $.get(urlGet,
        {
          mode: 'kelas_mentor',
          mentorId: "<?=$_SESSION['uid']?>"
        }, function(data, textStatus, xhr) {
        var dat = JSON.parse(data);
        var act = dat[1].data;

        for(var i=0; i<act.length; i++){
          $(".table1 tbody").append(
            pattern(1, (i+1), act[i])
          );
        }
      });

      $(".table1 table tbody").on('click', 'a', function(event) {
        window.open("http://localhost/Mentoring/index.php/VirtualMobile/kader?nama_kelas="+$(this).data('namakelas')+"&kelasId="+$(this).data('kelas'), "_self");
      });

      function pattern(mode, idx, data){
        switch (mode) {
          case 1:
            var patternRes =
              $("<tr>").
                append(
                  $("<td>").text(idx),
                  $("<td>").text(data.nama_kelas),
                  $("<td>").append(
                    $("<a>")
                      .text("Detail")
                      .attr({
                        'data-kelas': data.id,
                        'data-namakelas': data.nama_kelas,
                        'href': 'javascript:void(0)'})
                  )
                );
          break;
        }
        return patternRes;
      }
    </script>

  </body>
</html>
