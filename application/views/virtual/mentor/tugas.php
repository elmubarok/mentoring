<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Aktifitas</title>
    <script
      src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js"
      type="text/javascript"></script>
    <style media="screen">
      body.active{
        overflow: hidden;
      }
      button{
        margin: 12px 6px;
      }
      div{
        margin-bottom: 16px;
      }
      table, th, td{
        border: 1px solid #333;
        border-collapse: collapse;
        padding: 4px 12px;
      }
      .table2{
        position: fixed;
        width: 100%; height: 100%;
        left: 0; top: 0;
        background: #fff;
        visibility: hidden;
        overflow: auto;
      }
      .table2 div{
        padding: 8px;
      }
      .table2.active{
        visibility: visible;
      }
    </style>
  </head>
  <body>

    <h1>Tugas Kader Mentoring Saya <br> <small>Kelas <?=$_GET['nama_kelas']?></small> </h1>

    <div class="table1">
      <table>
        <thead>
          <th>No</th>
          <th>Tipe</th>
          <th>Surah/Materi</th>
          <th></th>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

    <script>
      var urlGet =
        'http://localhost/Mentoring/service/mobile/latest/mentor.php';

      $.get(urlGet,
        {
          mode: 'tugas_mentor',
          kelasId: "<?=$_GET['kelasId']?>"
        }, function(data, textStatus, xhr) {
        var dat = JSON.parse(data);
        var act = dat[1].data;

        for(var i=0; i<act.length; i++){
          $(".table1 tbody").append(
            pattern(1, (i+1), act[i])
          );
        }
      });

      $(".table1 table tbody").on('click', 'a', function(event) {
        var conf = confirm("Approve it ?");
        if(conf == true){
          $.get(urlGet,
            {
              mode: 'approve',
              kaderId: $(this).data('kader'),
              materiId: $(this).data('materi'),
              tgl: "<?=date('Y-m-d')?>"
            }, function(data, textStatus, xhr) {
              console.log(data);
          });
          alert("Approved!");
        }else{

        }
      });

      function pattern(mode, idx, data){
        switch (mode) {
          case 1:
            var patternRes =
              $("<tr>").
                append(
                  $("<td>").text(idx),
                  $("<td>").text(
                    (data.is_surat == 1) ? 'Hafalan Surat' : (data.is_hadist == 1) ? 'Hafalan Hadist' : ''
                  ),
                  $("<td>").text(data.nama_materi),
                  $("<td>").append(
                    $("<a>")
                      .text("Approve")
                      .attr({
                        'data-kader': data.kader_id,
                        'data-materi': data.materi_id,
                        'href': 'javascript:void(0)'})
                  )
                );
          break;
        }
        return patternRes;
      }
    </script>

  </body>
</html>
