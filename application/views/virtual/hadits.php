<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Hafalan Hadits</title>
    <script
      src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js"
      type="text/javascript"></script>
    <style media="screen">
      .form{
        display: block;
        padding: 2px 6px;
        text-transform: capitalize;
      }
      .forms1, .forms2{
        margin-bottom: 10px;
      }
      button{
        margin: 12px 6px;
      }
    </style>
  </head>
  <body>
    <?php
    ?>

    <div class="btns">
      <button onclick="window.open('<?=site_url('VirtualMobile/aktifitas')?>', '_self')">Aktifitas Saya</button>
      <button onclick="window.open('<?=site_url('VirtualMobile')?>', '_self')">Amaliyah</button>
      <button onclick="window.open('<?=site_url('VirtualMobile/quran')?>', '_self')">Hafalan Quran</button>
      <button onclick="window.open('<?=site_url('VirtualMobile/hadits')?>', '_self')">Hafalan Hadits</button>
    </div>

    <h1>Hafalan Hadits</h1>

    <form class="forms1"></form>

    <button id="btnSave">Simpan</button>

    <script>
      var urlCreate = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=insert';
      var urlGetHadits = 'http://localhost/Mentoring/service/mobile/latest/amaliyah.php?mode=hadits';

      $.get(urlGetHadits, function(data, textStatus, xhr) {
        var dat = JSON.parse(data);
        var hadits = dat[1].data;
        console.log(hadits[0].id);

        for(var i=0; i<hadits.length; i++){
          $(".forms1").append(
            pattern(hadits[i].id, hadits[i].nama)
          );
        }
      });

      $("#btnSave").click(function(event) {
        var data = $(".forms1").serialize();
        var kader = "&kader_id=4";
        var which = "&which=hadits";

        $.post(urlCreate, data+kader+which,
          function(data, textStatus, xhr) {
          var dat = JSON.parse(data)[0];
          if(dat.stat){
            location.reload();
          }else{
            alert("Error: look at network tools");
          }
        });
      });

      function pattern(data, caption){
        var patternRes =
          $("<div>")
            .attr({
              'class': 'form'
            })
            .append(
              $("<input>")
                .attr({
                  'type': 'checkbox',
                  'name': 'hadits[]',
                  'id': data,
                  'value': data
                }),
              $("<label>")
                .attr({
                  'for': data
                })
                .text(caption)
            );
          return patternRes;
      }
    </script>

  </body>
</html>
