<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>

<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #9E9E9E;
    width: 100px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
    vertical-align: middle;
  }
  .pagination{
    margin-top: 5px;
  }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]:after{ display:none; }
  .dataTable > thead > tr > th:nth-child(1)[class*=sort]{ padding-right: 0px;}

  #modalEbook .modal-body{
    padding-top: 0px;
  }
  #ebookphoto{
    height: 150px;
    background-color: #eee;
    background-size: cover;
    background-position: center;
    margin-bottom: 15px;
  }
  #ebookphoto button{
    position: absolute;
    right: 15px;
    bottom: 15px;
  }
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">

    <div class="form-group">
      <button class="btn btn-info btn-fill" id="btnAdd">
        <i class="fa fa-plus"></i>&nbsp;Tambah
      </button>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px;">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px">No</th>
              <th style="width: 100px;"></th>
              <th>Judul</th>
              <th>Konten</th>
              <th style="width: 100px;"></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="pull-right pagin-area"></div>

  </div>

</div>

<script>

  jQuery(document).ready(function($) {
    $('#editor').summernote({
      height: 300,
      minHeight: 300
    });

    $("#ebookphoto button").click(function(event) {
      $("#ebookcover").trigger('click');
    });

    $("#ebookcover").change(function(event) {
      readURL(this);
    });

    function readURL(input) {
      console.log(input);
      if (input.files && input.files[0]) {
        var reader = new FileReader;
        reader.onload = function(e) {
          $("#ebookphoto").css("background-image", "url("+e.target.result+")");
          $("#inpebookcover").val(e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
  });

  // urls
  var urlGetEbook = serviceURL + "f_ebook.php?action=getAllEbook";
  var urlGetEbookOne = serviceURL + "f_ebook.php?action=getEbookById";
  var urlUpdate = serviceURL + "f_ebook.php?action=update";
  var urlCreate = serviceURL + "f_ebook.php?action=create";
  var urlDel = serviceURL + "f_ebook.php?action=delete";
  var urlUpload = serviceURL + "uploadImage.php";

  var urlImgEbook = "<?=base_url('assets/images/')?>";

  jQuery(document).ready(function($) {
    initOptInsideModal();
  });

  var dTable = $("#tblData").DataTable({
    ajax: {
      url: urlGetEbook
    },
    'dom': 'rtp',
		'pageLength': 10,
    columnDefs: [{
			targets: [ 0, 1, 4 ],
			searchable: false,
      orderable: false
		}],
    columns: [
      {"data": null},
      {
        render: function(data, type, row){
          var img = row.image;
          var link = assetsURL + "images/" +img;
          return '<div class="img-container" style="background-image: url('+link+');"></div>'
        }
      },
      {"data": "title"},
      {
        render: function(data, type, row){
          var pure = row.content.replace(/<(?:.|\n)*?>/gm, '');
          return pure.length > 50 ?
            pure.substr( 0, 80 ) +'…' :
            pure;
        }
      },
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.id+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id="'+row.id+'"><i class="fa fa-times"></i></div></div>'
        }
      }
    ]
  });

  dTable.on('order.dt search.dt', function(){
		dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
	}).draw();

  $(".input-src").keyup(function(e) {
		dTable.search($(this).val()).draw();
	});

  $(".pagin-area").append($("#tblData_paginate"));

  $("#btnAdd").click(function(event) {
    $("#modalEbook .modal-title").text("Tambah Data Baru");
    modalFlush();
    $("#modalEbook").modal('show');

    $("#modalEbook .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmEbook").serialize();
      createEbook(serialData);
      console.log(serialData);
    });
  });

  $(".table tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalEbook .modal-title").text("Edit");

    setEditedModal($(this));

    $("#modalEbook").modal('show');

    $("#modalEbook .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmEbook").serialize();
      updateEbook(serialData);
      console.log(serialData);
    });
  });
  $(".table tbody").on('click', '.btn-rem', function(event) {
    event.preventDefault();

    $("#modalUsrRem").modal('show');

    var el = $(this);

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModal(el);
    });
  });

  function pattern(which, data){
    var patternRes = "";

    switch(which){
      case 1:
        patternRes =
        $("<tr>")
          .append(
            $("<td>").html( // img
              $("<div>")
                .attr({
                  'class': 'img-container',
                  'style': 'background-image: url(<?=base_url('assets/img/no-photo.png')?>)'
                })
            ),
            $("<td>").text(data.nama_lengkap), // nama
            $("<td>").text(data.hp), // hp
            $("<td>").text(data.email), // email
            $("<td>").text(data.jkelamin), // jenis kelamin
            $("<td>")
              .attr({'style': 'text-align: right'})
              .html(
                $("<div>")
                  .attr({'class': 'btn-group'})
                  .append(
                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                        'title': 'Edit',
                        'data-id': data.id,
                        'data-wajihah': data.wajihah_id,
                        'data-fakultas': data.fakultas_id,
                        'data-jurusan': data.jurusan_id,
                        'data-jabatan': data.jabatan_id,
                        'data-level': data.level,
                        'data-kelamin': data.jkelamin
                      })
                      .html($("<i>").attr({'class': 'fa fa-edit'})),

                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                        'title': 'Remove',
                        'data-id': data.id
                      })
                      .html($("<i>").attr({'class': 'fa fa-times'}))
                  ) // append
              )
        );
      break;
      case 2:
        patternRes =
        $("<option>")
          .attr({
            'value': data.id
          })
          .text(data.nama);
      break;
    }
    return patternRes;
  }

  function initOptInsideModal(){
    /*ldkList
      jurusanList
      fakultasList
      jabatanList
      levelList*/

    $("#ldkList, #jurusanList, #fakultasList, #jabatanList, #levelList, #nim, #angkatan")
      .parents(".form-group")
      .remove();
  }

  function setEditedModal(el){
    var id = el.data('id');

    $.get(urlGetEbookOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;

      $("#inpIdEbook").val(id);
      $("#titleEbook").val(edit.title);
      $('#editor').summernote('code', edit.content);
      $("#editor").html(edit.content);
      $("#ebookphoto").css("background-image", "url("+urlImgEbook+edit.image+")");
      $("#inpebookcovername").val(edit.image);

      console.log(edit);
    });
  }

  function setDelModal(el){
    var id = el.data('id');

    $.get(urlDel, {id:id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }

  function updateEbook(datas){
    $.post(urlUpdate, datas + "&level=3", function(data, textStatus, xhr) {
      if(data == 'Success'){
        if($("#inpebookcover").val() != ""){
          // cover has changed
          console.log("Cover has changed");
          var id = $("#inpIdEbook").val();
          $.post(urlUpload,
            {
              ebook: 'true',
              avatarName: id,
              avatar: $("#inpebookcover").val()
            }, function(dataa, textStatus, xhr) {
              var dats = JSON.parse(dataa);
              if(dats.message == 'Image saved to server'){
                location.reload();
              }else{
                alert("Error, please check the console for details.")
              }
          });
        }else{
          location.reload();
        }
      }
    });
  }

  function createEbook(datas){
    $.post(urlCreate, datas, function(data, textStatus, xhr) {
      var dat = JSON.parse(data);
      if(dat[0].stat == 'Success'){
        if($("#inpebookcover").val() != ""){
          // cover has changed
          var id = dat[0].id;
          $.post(urlUpload,
            {
              ebook: 'true',
              avatarName: id,
              avatar: $("#inpebookcover").val()
            }, function(dataa, textStatus, xhr) {
              var dats = JSON.parse(dataa);
              if(dats.message == 'Image saved to server'){
                location.reload();
              }else{
                alert("Error, please check the console for details.")
              }
          });
        }else{
          location.reload();
        }
      }
    });
  }

  function modalFlush(){
    $("#modalEbook input, #modalPengumuman textarea").val('');
    $("#editor").summernote('reset');
    $("#ebookphoto").css("background-image", "url(<?=base_url('assets/img/full-screen-image-3.jpg')?>)");
  }
</script>

<!--
SUPER ADMINISTRATOR Can :
- edit superadministrator data
- CRUD admin data
- CRUD mentoring class
- CRUD mentor
- CRUD kader
- view mentoring report
-->
