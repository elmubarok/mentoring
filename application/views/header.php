<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<title>M - MENTORING</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Animation library for notifications   -->
	<link href="<?php echo base_url()?>assets/css/animate.min.css" rel="stylesheet"/>
	<!--  Light Bootstrap Table core CSS    -->
	<link href="<?php echo base_url()?>assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="<?php echo base_url()?>assets/css/demo.css" rel="stylesheet" />

	<!--     Fonts and icons     -->
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url()?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

	<!-- Datatables -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

	<!-- Js Script -->
	<script src="<?php echo base_url()?>assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/js/serviceurl.js" type="text/javascript"></script>

	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

	<link rel="stylesheet" href="<?=base_url()?>/assets/css/croppie.css" />
	<script src="<?=base_url()?>/assets/js/croppie.min.js"></script>
</head>
<style media="screen">
	.btn-circle.btn-xl {
			width: 70px;
			height: 70px;
			padding: 10px 16px;
			border-radius: 35px;
			font-size: 24px;
			line-height: 1.33;
	}

	.btn-circle {
			width: 30px;
			height: 30px;
			padding: 6px 0px;
			border-radius: 15px;
			text-align: center;
			font-size: 12px;
			line-height: 1.42857;
	}
</style>
<body>

	<div class="wrapper">
		<div class="sidebar" data-color="purple" data-image="">
			<!--   you can change the color of the sidebar
			using: data-color="blue | azure | green | orange | red | purple" -->
			<div class="sidebar-wrapper">
				<div class="logo">
					<a href="" class="simple-text">
						M - MENTORING
					</a>
				</div>

				<ul class="nav">
					<!-- super:1, admin:2 -->
					<?php if(isset($_SESSION['level'])): ?>

					<!-- Superadmin -->
					<?php if($_SESSION['level'] == 1): ?>
					<li class="<?=($type == 1) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/admin'?>">
							<i class="pe-7s-science"></i>
							<p>Superadmin</p>
						</a>
					</li>
					<?php endif; ?>
					<?php if($_SESSION['level'] == 1 || $_SESSION['level'] == 2): ?>
					<li class="<?=($type == 2) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/admin?none'?>">
							<i class="pe-7s-science"></i>
							<p>Admin</p>
						</a>
					</li>
					<?php endif; ?>
					<!-- /Superadmin -->

					<!-- Mentor -->
					<?php $level = $_SESSION['level']; ?>
					<?php if($_SESSION['level'] == 2 || $_SESSION['level'] == 1): ?>
					<li class="<?=($type == 3) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/kelas'?>">
							<i class="pe-7s-user"></i>
							<p><?=($level == 4) ? "" : "Kelola" ?> Kelas</p>
						</a>
					</li>
					<li class="<?=($type == 4) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/pengumuman'?>">
							<i class="pe-7s-note2"></i>
							<p><?=($level == 4) ? "" : "Kelola" ?> Pengumuman</p>
						</a>
					</li>
					<li class="<?=($type == 5) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/ebook'?>">
							<i class="pe-7s-news-paper"></i>
							<p><?=($level == 4) ? "" : "Kelola" ?> E - Book</p>
						</a>
					</li>
					<?php endif; ?>
					<!-- /Mentor -->

					<!-- Admin -->
					<?php if($_SESSION['level'] == 2 || $_SESSION['level'] == 1): ?>
					<li class="<?=($type == 6) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/mentor'?>">
							<i class="pe-7s-science"></i>
							<p>Kelola Mentor</p>
						</a>
					</li>
					<li class="<?=($type == 7) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/kader'?>">
							<i class="pe-7s-science"></i>
							<p>Kelola Kader</p>
						</a>
					</li>
					<!-- <li class="<?=($type == 8) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/kaderisasi'?>">
							<i class="pe-7s-science"></i>
							<p>Kelola Kaderisasi</p>
						</a>
					</li> -->
					<li class="<?=($type == 9) ? 'active' : '';?>">
						<a href="<?php echo base_url().'index.php/rekap'?>">
							<i class="pe-7s-science"></i>
							<p>Data Mentoring</p>
						</a>
					</li>
					<?php endif; ?>
					<!-- /Admin -->
					<?php endif; ?>

				</ul>
			</div>
			<div class="sidebar-background" style="background-image: url(<?=base_url()?>/assets/img/sidebar-4.jpg) "></div>
		</div>

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#" style="text-transform: uppercase;">
							<?=$page_title?>
						</a>
					</div>

					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<!-- <li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-dashboard"></i>
									<p class="hidden-lg hidden-md">Dashboard</p>
								</a>
							</li> -->

							<!-- <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-globe"></i>
									<b class="caret hidden-sm hidden-xs"></b>
									<span class="notification hidden-sm hidden-xs">5</span>
									<p class="hidden-lg hidden-md">
										5 Notifications
										<b class="caret"></b>
									</p>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Notification 1</a></li>
									<li><a href="#">Notification 2</a></li>
									<li><a href="#">Notification 3</a></li>
									<li><a href="#">Notification 4</a></li>
									<li><a href="#">Another notification</a></li>
								</ul>
							</li>

							<li>
								<a href="">
									<i class="fa fa-search"></i>
									<p class="hidden-lg hidden-md">Search</p>
								</a>
							</li> -->
						</ul>

						<ul class="nav navbar-nav navbar-right">
							<!-- <li>
								<a href="">
									<p>Account</p>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<p>
										Dropdown
										<b class="caret"></b>
									</p>

								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</li> -->

							<li>
								<a href="javascript:void(0)" id="outBtn">
									<p>Log out</p>
								</a>
							</li>
							<li class="separator hidden-lg hidden-md"></li>
						</ul>
					</div>
				</div>
			</nav>
