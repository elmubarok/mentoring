<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #ababab;
    width: 50px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
  }
  .pagination{
    margin-top: 5px;
  }
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">
    <div class="form-group">
      <button class="btn btn-info btn-fill" id="btnAdd">
        <i class="fa fa-plus"></i>&nbsp;Tambah
      </button>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari kelas ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px;">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px;">No</th>
              <th>Nama Kelas</th>
              <th>Kader</th>
              <th></th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>

    <div class="pull-right pagin-area"></div>

  </div>

</div>

<script>
  // urls
  var urlGetGroupingAll = serviceURL + "f_grouping.php?action=getGroupingAll";
  var urlGetGroupingOne = serviceURL + "f_grouping.php?action=getGroupingById";
  var urlUpdate = serviceURL + "f_grouping.php?action=updateGrouping";
  var urlCreate = serviceURL + "f_grouping.php?action=create";
  var urlDel = serviceURL + "f_grouping.php?action=delete"; // not yet
  var urlGetKelasAll = serviceURL + "f_grouping.php?action=getKelasAll";
  var urlGetKaderAll = serviceURL + "f_grouping.php?action=getKaderAll";

  initOptInsideModal(1);
  initOptInsideModal(2);

  const kelas_uid = <?=json_encode($nama_kelas)?>;
  console.log(kelas_uid[0].kelas_id);

  // $.get(urlGetGroupingAll, function(data, textStatus, xhr) {
  //   var res = JSON.parse(data)['items'];
  //     for(var i=0; i<res.length; i++){
  //       $("#tblData").append(pattern(1,res[i]));
  //     }
  // });

  var dTable = $("#tblData").DataTable({
    ajax: {
      url: urlGetGroupingAll
    },
    'dom': 'rtp',
		'pageLength': 2,
    columnDefs: [{
			targets: [ 0, 2, 3 ],
			searchable: false,
      orderable: false
		}],
    columns: [
      {"data": null},
      {"data": "nama_kelas"},
      {"data": "nama_kader"},
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.grouping_id+'" data-kelas="'+row.kelas_id+'" data-kader="'+row.kader_id+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id='+row.id+'><i class="fa fa-times"></i></div></div>'
        }
      }
    ]
  });

  dTable.on('order.dt search.dt', function(){
		dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
	}).draw();

  $(".input-src").keyup(function(e) {
		dTable.column(1).search($(this).val()).draw();
	});

  $(".pagin-area").append($("#tblData_paginate"));

  $("#btnAdd").click(function(event) {
    $("#modalGrouping .modal-title").text("Tambah Data Baru");
    modalFlush();
    $("#modalGrouping").modal('show');

    $("#modalGrouping .modal-footer").on('click', '#btnKelasSave', function(event) {
      var serialData = $("#frmGroupingData").serialize();
      createKelas(serialData);
      console.log(serialData);
    });
  });

  $(".table tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalGrouping .modal-title").text("Edit");

    $("#mentorList").val($(this).data('mentor'));

    setEditedModal($(this));

    $("#modalGrouping").modal('show');

    $("#modalGrouping .modal-footer").on('click', '#btnKelasSave', function(event) {
      var serialData = $("#frmGroupingData").serialize();
      updateUser(serialData);
      console.log(serialData);
    });
  });
  $(".table tbody").on('click', '.btn-rem', function(event) {
    event.preventDefault();

    var el = $(this);
    $("#modalUsrRem").modal('show');

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModal(el);
    });
  });

  function pattern(which, data){
    var patternRes = "";

    switch(which){
      case 1:
        patternRes =
        $("<tr>")
          .append(
            $("<td>").text(data.nama_kelas), // nama
            $("<td>").text(data.nama_kader), // kader
            $("<td>")
              .attr({'style': 'text-align: right'})
              .html(
                $("<div>")
                  .attr({'class': 'btn-group'})
                  .append(
                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                        'title': 'Edit',
                        'data-id': data.grouping_id,
                        'data-kelas': data.kelas_id,
                        'data-kader': data.kader_id
                      })
                      .html($("<i>").attr({'class': 'fa fa-edit'})),

                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                        'title': 'Remove',
                        'data-id': data.id
                      })
                      .html($("<i>").attr({'class': 'fa fa-times'}))
                  ) // append
              )
        );
      break;
      case 2:
        patternRes =
        $("<option>")
          .attr({
            'value': data.id
          })
          .text(data.nama);
      break;
      case 3:
        patternRes =
          $("<div>")
            .attr({'class': 'btn-group'})
            .append(
              $("<div>")
                .attr({
                  'type': 'button',
                  'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                  'title': 'Edit',
                  'data-id': "data.grouping_id",
                  'data-kelas': "data.kelas_id",
                  'data-kader': "data.kader_id"
                })
                .html($("<i>").attr({'class': 'fa fa-edit'})),

              $("<div>")
                .attr({
                  'type': 'button',
                  'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                  'title': 'Remove',
                  'data-id': "data.id"
                })
                .html($("<i>").attr({'class': 'fa fa-times'}))
            ); // append
      break;
    }
    return patternRes;
  }

  function initOptInsideModal(which){
    /*ldkList
      jurusanList
      fakultasList
      jabatanList
      levelList*/

    switch(which){
      case 1:
        $.get(urlGetKaderAll, function(data, textStatus, xhr) {
          var dat = JSON.parse(data).items;
          for(var i=0; i<dat.length; i++){
            $("#kaderList").append(pattern(2, dat[i]))
          }
        });
      break;
      case 2:
        $.get(urlGetKelasAll, function(data, textStatus, xhr) {
          var dat = JSON.parse(data).items;
          for(var i=0; i<dat.length; i++){
            $("#kelasList").append(pattern(2, dat[i]))
          }
        });
      break;
    }
  }

  function setEditedModal(el){
    var id = el.data('id');

    $.get(urlGetGroupingOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;

      $("#inpIdGroup").val(id);
      $("#kaderList").val(edit.kader_id);
      $("#kelasList").val(edit.kelas_id);

      console.log(edit);
    });
  }

  function setDelModal(el){
    var id = el.data('id');

    $.get(urlDel, {id:id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }

  function updateUser(data){
    $.post(urlUpdate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function createKelas(data){
    $.post(urlCreate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function modalFlush(){
    $("#modalGrouping input").val('');
    $("#modalGrouping select").val('null');
  }
</script>

<!--
SUPER ADMINISTRATOR Can :
- CRUD superadministrator data - edit, delete, read, create [ALL DONE]
- CRUD admin data - edit, delete, read, create [ALL DONE]
- CRUD mentoring class - edit, read, create, delete [DONE]
- CRUD mentor - edit, read, create, delete [ALL DONE]
- CRUD kader
- view mentoring report
-->
