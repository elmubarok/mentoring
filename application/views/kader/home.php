<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
  .img-container{
    border-radius: 4px;
    border: 1px solid #ababab;
    width: 50px;
    height: 50px;
    background-position: center;
    background-size: cover;
  }
  .table tbody tr td{
    text-transform: capitalize;
    vertical-align: middle;
  }
  .pagination{
    margin-top: 5px;
  }
</style>
<div id="page-wrapper" class="content">

  <div class="container-fluid">
    <div class="form-group">
      <button class="btn btn-info btn-fill" id="btnAdd">
        <i class="fa fa-plus"></i>&nbsp;Tambah
      </button>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="text" class="form-control input-sm input-src" placeholder="Cari ...">
      </div>
    </div>

    <div class="card" style="margin-bottom: 0px">
      <div class="card-body">
        <table class="table table-hover table-striped table-with-thumb" id="tblData" style="width: 100%;">
          <thead>
            <tr>
              <th style="width: 10px;">No</th>
              <th style="width:50px;"></th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Fakultas</th>
              <th>Jurusan</th>
              <th>Wajihah</th>
              <th>Jenis Kelamin</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="pull-right pagin-area"></div>

  </div>

</div>

<script>
  // urls
  var urlGetKader = serviceURL + "f_kader.php?action=getUserAll";
  var urlGetKaderOne = serviceURL + "f_kader.php?action=getUserById";
  var urlGetLdk = serviceURL + "f_kader.php?action=getLdkAll";
  var urlGetJur = serviceURL + "f_kader.php?action=getJurusanAll";
  var urlGetFak = serviceURL + "f_kader.php?action=getFakultasAll";
  var urlGetJab = serviceURL + "f_kader.php?action=getJabatanAll";
  var urlGetLev = serviceURL + "f_kader.php?action=getLevelAll";
  var urlUpdate = serviceURL + "f_kader.php?action=updateUser";
  var urlCreate = serviceURL + "f_kader.php?action=create";
  var urlDel = serviceURL + "f_kader.php?action=delete";

  initOptInsideModal(1);
  initOptInsideModal(2);
  initOptInsideModal(3);
  initOptInsideModal(4);
  initOptInsideModal(5);

  // $.get(urlGetKader, function(data, textStatus, xhr) {
  //   var res = JSON.parse(data)['items'];
  //   for(var i=0; i<res.length; i++){
  //     $("#tblList").append(pattern(1, res[i]));
  //   }
  // });

  var dTable = $("#tblData").DataTable({
    ajax: {
      url: urlGetKader
    },
    'dom': 'rtp',
		'pageLength': 10,
    columnDefs: [{
			targets: [ 0, 1, 6 ],
			searchable: false,
      orderable: false
		}],
    columns: [
      {"data": null},
      {
        render: function(data, type, row){
          return '<div class="img-container" style="background-image: url(http://localhost/Mentoring/assets/img/'+row.avatar+')"></div>'
        }
      },
      {"data": "nim"},
      {"data": "nama_lengkap"},
      {"data": "fakultas"},
      {"data": "jurusan"},
      {"data": "wajihah"},
      {"data": "jkelamin"},
      {
        render: function(data, type, row){
          return '<div class="btn-group pull-right"><div type="button" class="btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit" title="Edit" data-id="'+row.id+'" data-wajihah="'+row.wajihah_id+'" data-fakultas="'+row.fakultas_id+'" data-jurusan="'+row.jurusan_id+'" data-jabatan="'+row.jabatan_id+'" data-level="'+row.level+'" data-kelamin="'+row.jkelamin+'"><i class="fa fa-edit"></i></div><div type="button" class="btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem" title="Remove" data-id="'+row.id+'"><i class="fa fa-times"></i></div></div>'
        }
      }
    ]
  });

  dTable.on('order.dt search.dt', function(){
		dTable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){cell.innerHTML = i+1});
	}).draw();

  $(".input-src").keyup(function(e) {
		dTable.search($(this).val()).draw();
	});

  $(".pagin-area").append($("#tblData_paginate"));

  $("#btnAdd").click(function(event) {
    $("#modalUsrEdit .modal-title").text("Tambah Data Baru");
    modalFlush();
    $("#modalUsrEdit").modal('show');

    $("#modalUsrEdit .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmAdminData").serialize();
      createUser(serialData);
      console.log(serialData);
    });
  });

  $(".table tbody").on('click', '.btn-edit', function(event) {
    event.preventDefault();

    $("#modalUsrEdit .modal-title").text("Edit");

    $("#ldkList").val($(this).data('wajihah'));
    $("#jurusanList").val($(this).data('jurusan'));
    $("#fakultasList").val($(this).data('fakultas'));
    $("#jabatanList").val($(this).data('jabatan'));
    $("#levelList").val($(this).data('level'));
    $("#jkList").val($(this).data('kelamin'));

    setEditedModal($(this));

    $("#modalUsrEdit").modal('show');

    $("#modalUsrEdit .modal-footer").on('click', '#btnAdminSave', function(event) {
      var serialData = $("#frmAdminData").serialize();
      updateUser(serialData);
      console.log(serialData);
    });
  });
  $(".table tbody").on('click', '.btn-rem', function(event) {
    event.preventDefault();

    $("#modalUsrRem").modal('show');
    var el = $(this);

    $("#modalUsrRem").on('click', '#btnAdminDel', function(event) {
      setDelModal(el);
    });
  });

  function pattern(which, data){
    var patternRes = "";

    switch(which){
      case 1:
        patternRes =
        $("<tr>")
          .append(
            $("<td>").html( // img
              $("<div>")
                .attr({
                  'class': 'img-container',
                  'style': 'background-image: url(<?=base_url('assets/img/no-photo.png')?>)'
                })
            ),
            $("<td>").text(data.nim), // nim
            $("<td>").text(data.nama_lengkap), // nama
            $("<td>").text(data.fakultas), // ft
            $("<td>").text(data.jurusan), // jurusan
            $("<td>").text(data.wajihah), // wajihah
            $("<td>").text(data.jabatan), // jabatan
            $("<td>")
              .attr({'style': 'text-align: right'})
              .html(
                $("<div>")
                  .attr({'class': 'btn-group'})
                  .append(
                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-warning btn-simple btn-icon btn-fill btn-xs btn-edit',
                        'title': 'Edit',
                        'data-id': data.id,
                        'data-wajihah': data.wajihah_id,
                        'data-fakultas': data.fakultas_id,
                        'data-jurusan': data.jurusan_id,
                        'data-jabatan': data.jabatan_id,
                        'data-level': data.level,
                        'data-kelamin': data.jkelamin
                      })
                      .html($("<i>").attr({'class': 'fa fa-edit'})),

                    $("<div>")
                      .attr({
                        'type': 'button',
                        'class': 'btn btn-danger btn-simple btn-icon btn-fill btn-xs btn-rem',
                        'title': 'Remove',
                        'data-id': data.id
                      })
                      .html($("<i>").attr({'class': 'fa fa-times'}))
                  ) // append
              )
        );
      break;
      case 2:
        patternRes =
        $("<option>")
          .attr({
            'value': data.id
          })
          .text(data.nama);
      break;
    }
    return patternRes;
  }

  function initOptInsideModal(which){
    /*ldkList
      jurusanList
      fakultasList
      jabatanList
      levelList*/

    switch(which){
      case 1:
        $.get(urlGetLdk, function(data, textStatus, xhr) {
          var ldk = JSON.parse(data).items;
          for(var i=0; i<ldk.length; i++){
            $("#ldkList").append(pattern(2, ldk[i]))
          }
        });
      break;
      case 2:
        $.get(urlGetJur, function(data, textStatus, xhr) {
          var jur = JSON.parse(data).items;
          for(var i=0; i<jur.length; i++){
            $("#jurusanList").append(pattern(2, jur[i]))
          }
        });
      break;
      case 3:
        $.get(urlGetFak, function(data, textStatus, xhr) {
          var fak = JSON.parse(data).items;
          for(var i=0; i<fak.length; i++){
            $("#fakultasList").append(pattern(2, fak[i]))
          }
        });
      break;
      case 4:
        $.get(urlGetJab, function(data, textStatus, xhr) {
          var jab = JSON.parse(data).items;
          for(var i=0; i<jab.length; i++){
            $("#jabatanList").append(pattern(2, jab[i]))
          }
        });
      break;
      case 5:
        $.get(urlGetLev, function(data, textStatus, xhr) {
          var lev = JSON.parse(data).items;
          for(var i=0; i<lev.length; i++){
            $("#levelList").append(pattern(2, lev[i]))
          }
        });
      break;
    }
  }

  function setEditedModal(el){
    var id = el.data('id');

    $.get(urlGetKaderOne, {id:id}, function(data, textStatus, xhr) {
      var edit = JSON.parse(data).item;

      $("#inpId").val(edit.id);
      $("#callname").val(edit.nama_panggilan);
      $("#fullname").val(edit.nama_lengkap);
      $("#addrown").val(edit.alamat_asal);
      $("#addrdom").val(edit.alamat_domisili);
      $("#hp").val(edit.hp);
      $("#email").val(edit.email);
      $("#nim").val(edit.nim);
      $("#angkatan").val(edit.angkatan);
      $("#uname").val(edit.username);
      $("#upswd").val(edit.password);

      console.log(edit);
    });
  }

  function setDelModal(el){
    var id = el.data('id');

    $.get(urlDel, {id:id}, function(data, textStatus, xhr) {
      if(data == "1"){
        location.reload();
      }
    });
  }

  function updateUser(data){
    $.post(urlUpdate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function createUser(data){
    $.post(urlCreate, data, function(data, textStatus, xhr) {
      if(data == 'Success'){
        location.reload();
      }
    });
  }

  function modalFlush(){
    $("#modalUsrEdit input").val('');
    $("#modalUsrEdit select").val('null');
    $("#levelList").val('4');
  }
</script>

<!--
SUPER ADMINISTRATOR Can :
- CRUD superadministrator data - edit, delete, read, create [ALL DONE]
- CRUD admin data - edit, delete, read, create [ALL DONE]
- CRUD mentoring class - edit, read, create, delete [DONE]
- CRUD mentor - edit, read, create, delete [ALL DONE]
- CRUD kader
- view mentoring report
-->
