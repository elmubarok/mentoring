</div>
</body>

<div class="modal fade" tabindex="-1" role="dialog" id="modalUsrEdit">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit <b id="usrEditNameTitle"></b> </h4>
      </div>
      <div class="modal-body">

        <div id="imgAvatar" style="width: 150px; height: 150px; position: relative; background-image: url(http://localhost/Mentoring/assets/img/no-photo.png); background-size: cover; background-position: center; border-radius: 50%; border: 1px solid #ababab; box-shadow: 0px 2px 10px 0px rgba(138, 138, 138, 0.85); display: block; margin: 0 auto;">
          <div id="imgAvatarInnerContainer">
            <img src="" id="imgAvatarInner">
          </div>
          <div id="btnAvatarGroup" style="display: none;">
            <button id="btnUpAvatarCls" type="button" class="btn btn-danger btn-circle btn-lg" style="position: absolute; bottom: -12px; left: -12px; z-index: 100;">
              <i class="fa fa-times"></i>
            </button>
            <button id="btnUpAvatarUp" type="button" class="btn btn-success btn-circle btn-lg" style="position: absolute; bottom: -12px; right: -12px; z-index: 100;">
              <i class="fa fa-check"></i>
            </button>
          </div>
          <button id="btnFileAvatar" type="button" class="btn btn-info btn-circle btn-lg" style="position: absolute; bottom: 0; right: 0;">
            <i class="fa fa-camera"></i>
          </button>
        </div>

        <form id="frmAdminData" style="margin-top: 16px;">
          <input name="id" id="inpId" hidden>
          <input type="file" id="inputAvatar" style="display: none">

          <input id="inpAvatarFinal" name="avatar" hidden>
          <input type="text" id="avatarName" name="avatarName" id="avatarName" hidden>

          <div class="form-group">
            <label>LDK</label>
            <select name="ldkId" id="ldkList" class="form-control">
              <option disabled selected value="null">Pilih LDK</option>
            </select>
          </div>

          <div class="form-group">
            <label>Nama Panggilan</label>
            <input name="callname" id="callname" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Nama Lengkap</label>
            <input name="fullname" id="fullname" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Alamat Asal</label>
            <input name="addrown" id="addrown" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Alamat Domisili</label>
            <input name="addrdom" id="addrdom" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>HP</label>
            <input name="hp" id="hp" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input name="email" id="email" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>NIM</label>
            <input name="nim" id="nim" class="form-control" placeholder="">
          </div>

          <div class="form-group">
            <label>Pilih Jurusan</label>
            <select name="jurusanId" id="jurusanList" class="form-control">
              <option disabled selected value="null">Pilih Jurusan</option>
            </select>
          </div>
          <div class="form-group">
            <label>Pilih Fakultas</label>
            <select name="fakultasId" id="fakultasList" class="form-control">
              <option disabled selected value="null">Pilih Fakultas</option>
            </select>
          </div>

          <div class="form-group">
            <label>Angkatan</label>
            <input name="angkatan" id="angkatan" class="form-control" placeholder="">
          </div>

          <div class="form-group">
            <label>Pilih Jabatan</label>
            <select name="jabatanId" id="jabatanList" class="form-control">
              <option disabled selected value="null">Pilih Jabatan</option>
            </select>
          </div>
          <div class="form-group">
            <label>Pilih Jenis Kelamin</label>
            <select name="jkelamin" id="jkList" class="form-control">
              <option disabled selected value="null">Pilih Jenis Kelamin</option>
              <option value="l">Laki-laki</option>
              <option value="p">Wanita</option>
            </select>
          </div>

          <div class="form-group">
            <label>Username</label>
            <input name="uname" id="uname" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input name="upswd" id="upswd" class="form-control" placeholder="">
          </div>

          <div class="form-group">
            <label>Pilih Level</label>
            <select name="level" id="levelList" class="form-control">
              <option disabled selected value="null">Pilih Level</option>
            </select>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" id="btnAdminSave">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalUsrRem">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h3>Hapus Data Ini ??</h3>
        <div class="form-group" style="margin-top: 16px;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Nope</button>
          <button class="btn btn-danger" id="btnAdminDel">Yap</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalKelas">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit <b id="usrEditNameTitle"></b> </h4>
      </div>
      <div class="modal-body">

        <form id="frmKelasData">
          <input name="inpId" id="inpIdKelas" hidden placeholder="Enter text">
          <div class="form-group">
            <label>Nama Kelas</label>
            <input name="inpKelasName" id="inpKelasName" class="form-control" placeholder="Enter text">
          </div>

          <div class="form-group">
            <label>Tempat</label>
            <input name="inpLokasi" id="inpLokasi" class="form-control" placeholder="Enter text">
          </div>

          <div class="form-group">
            <label>Hari</label>
            <input name="inpHari" id="inpHari" class="form-control" placeholder="Enter text">
          </div>
          <div class="form-group">
            <label>Jam</label>
            <input name="inpJam" id="inpJam" class="form-control" placeholder="Enter text">
          </div>
          <div class="form-group">
            <label>Link Whatsapp</label>
            <input name="inpLinkWa" id="inpLinkWa" class="form-control" placeholder="Enter text">
          </div>
          <div class="form-group">
            <label>Jenis Kelas</label>
            <select name="jkelamin" id="jenisKelas" class="form-control">
              <option disabled selected value="null">Jenis</option>
              <option value="l">Laki-laki</option>
              <option value="p">Perempuan</option>
            </select>
          </div>
          <div class="form-group">
            <label>Pilih Mentor</label>
            <select name="inpMentorId" id="mentorList" class="form-control">
              <option disabled selected value="null">Pilih Mentor</option>
            </select>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" id="btnKelasSave">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalGrouping">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit <b id="usrEditNameTitle"></b> </h4>
      </div>
      <div class="modal-body">

        <form id="frmGroupingData">
          <input name="inpId" id="inpIdGroup" hidden placeholder="Enter text"/>
          <input name="kaderId" id="inpIdGroupKader" hidden placeholder="Enter text"/>
          <!-- <div class="form-group">
            <label>Pilih Kader</label>
            <select name="kaderId" id="kaderList" class="form-control">
              <option disabled selected value="null">Pilih Kader</option>
            </select>
          </div> -->
          <h3 id="kaderNama">Nama Kader</h3>
          <div class="form-group">
            <label>Pilih Kelas</label>
            <select name="kelasId" id="kelasList" class="form-control">
              <option disabled selected value="null">Pilih Kelas</option>
            </select>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" id="btnKelasSave">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalPengumuman">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit <b id="usrEditNameTitle"></b> </h4>
      </div>
      <div class="modal-body">

        <form id="frmPengumuman">
          <input name="inpId" id="inpIdP" hidden placeholder="Enter text"/>
          <div class="form-group">
            <label>Judul</label>
            <input type="text" name="inpTitle" id="judulP" class="form-control" placeholder="judul ...">
          </div>
          <div class="form-group">
            <label>Konten</label>
            <textarea name="inpContent" id="kontenP" class="form-control" placeholder="konten ..." style="min-width: 100%; max-width: 100%; min-height: 100px; max-height: 100%;"></textarea>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" id="btnAdminSave">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalEbook">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit <b id="usrEditNameTitle"></b> </h4>
      </div>
      <div class="modal-body">
        <input type="text" name="cover" id="inpebookcover" hidden>
        <form id="frmEbook">
          <div class="row">
            <div class="col-md-12" id="ebookphoto" style="background-image: url(<?=base_url('assets/img/full-screen-image-3.jpg')?>);">
              <button type="button" class="btn btn-default btn-sm"> <i class="fa fa-camera"></i> </button>
            </div>
          </div>
          <input type="file" accept="image/*" id="ebookcover" style="display: none;">

          <input type="text" name="covername" id="inpebookcovername" hidden>

          <input name="inpId" id="inpIdEbook" hidden placeholder="Enter text"/>
          <div class="form-group">
            <label>Judul</label>
            <input id="titleEbook" type="text" name="title" class="form-control">
          </div>
          <div class="form-group">
            <textarea id="editor" name="content" style="min-height: 400px;"></textarea>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" id="btnAdminSave">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalVUser">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- http://localhost/Mentoring/assets/img/avatar5.jpg -->
      <div class="modal-body">

        <div style="margin-bottom: 10px; width: 100px; height: 100px; position: relative; background-image: url(http://localhost/Mentoring/assets/img/avatar5.jpg); background-size: cover; background-position: center; border-radius: 50%; border: 1px solid #ababab; box-shadow: 0px 2px 10px 0px rgba(138, 138, 138, 0.85); display: block; margin: 0 auto;"></div>

        <div class="">
          <ul>
            <li></li>
          </ul>
        </div>

        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalDestroy">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h3>Ini akan menghapus semua data kegiatan kader, lanjutkan ?</h3>
        <div class="form-group" style="margin-top: 16px;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Nope</button>
          <button class="btn btn-danger destroy">Yap</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!--  Charts Plugin -->
<script src="<?php echo base_url()?>assets/js/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url()?>assets/js/bootstrap-notify.js"></script>
<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url()?>assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<script>
  $("#outBtn").click(function(){
    window.location.replace("<?=site_url('auth/keluar')?>");
  });
  /*
  inpId
  ldkList
  callname
  fullname
  addrown
  addrdom
  hp
  email
  nim
  jurusanList
  fakultasList
  angkatan
  jabatanList
  jkList
  uname
  upswd
  levelList
  */

  /*
  id
  ldkId
  callname
  fullname
  addrown
  addrdom
  hp
  email
  nim
  jurusanId
  fakultasId
  angkatan
  jabatanId
  jkelamin
  uname
  upswd
  level
  */

  /*
  inpId
  inpKelasName
  inpLokasi
  inpHari
  inpJam
  inpLinkWa
  mentorList
  */

  /*
  inpId
  inpKelasName
  inpLokasi
  inpHari
  inpJam
  inpLinkWa
  inpMentorId
  */
</script>
</html>
