<?php
class Admin extends CI_Controller{

	public function __construct(){

		parent::__construct();
		//$this->load->model('kelas_model');
		$this->load->helper('form');
	}

	public function index(){
		if(isset($_SESSION['uid'])){
			if($_SESSION['level'] == 1):
				$data['page_title'] =
					"kelola".((isset($_GET['none'])) ? " admin" : " superadmin");
				$data['type'] = (isset($_GET['none'])) ? "2" : "1";
			elseif($_SESSION['level'] == 2):
				$data['page_title'] =
					"kelola admin";
				$data['type'] = "2";
			endif;

			$this->load->view('header', $data);
			if($_SESSION['level'] == 1):
				if(!isset($_GET['none'])):
					$this->load->view('admin/home');
				else:
					$this->load->view('admin/admin');
				endif;
			elseif($_SESSION['level'] == 2):
				$this->load->view('admin/admin');
			endif;

			$this->load->view('footer');
		}else{
			redirect("auth");
		}
	}

}
