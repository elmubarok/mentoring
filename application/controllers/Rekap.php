<?php
class Rekap extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		if(isset($_SESSION['uid'])){
			$data['page_title'] = 'rekap data';
			$data['type'] = "9";

			$this->load->view('header', $data);
			$this->load->view('rekap/home');
			$this->load->view('footer');
		}else{
			redirect("auth");
		}
	}

}
