<?php
class Kelas extends CI_Controller{

	public function __construct(){

		parent::__construct();
		//$this->load->model('kelas_model');
		$this->load->helper('form');
	}

	public function index(){
		if(isset($_SESSION['uid'])){
			$data['page_title'] = 'kelola Kelas';
			$data['type'] = "3";

			$this->load->view('header', $data);
			$this->load->view('kelas/home');
			$this->load->view('footer');
		}else{
			redirect("auth");
		}
	}

}
