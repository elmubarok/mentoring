<?php
class Pengumuman extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		if(isset($_SESSION['uid'])){
			$data['page_title'] = 'kelola pengumuman';
			$data['type'] = "4";

			$this->load->view('header', $data);
			$this->load->view('pengumuman/home');
			$this->load->view('footer');
		}else{
			redirect("auth");
		}
	}

}

/*
select `amlservice`.`users`.`id` AS `id`,`amlservice`.`users`.`username` AS `username`,`amlservice`.`users`.`password` AS `password`,`amlservice`.`users`.`level` AS `level`,`amlservice`.`level`.`nama` AS `nama`,`amlservice`.`wajihah`.`nama` AS `wajihah`,`amlservice`.`users`.`nama_panggilan` AS `nama_panggilan`,`amlservice`.`users`.`nama_lengkap` AS `nama_lengkap`,`amlservice`.`users`.`hp` AS `hp`,`amlservice`.`users`.`email` AS `email`,`amlservice`.`users`.`avatar` AS `avatar`,`amlservice`.`users`.`alamat_asal` AS `alamat_asal`,`amlservice`.`users`.`alamat_domisili` AS `alamat_domisili`,`amlservice`.`users`.`nim` AS `nim`,`amlservice`.`jurusan`.`nama` AS `jurusan`,`amlservice`.`jabatan`.`nama` AS `jabatan`,`amlservice`.`fakultas`.`nama` AS `fakultas`,`amlservice`.`users`.`angkatan` AS `angkatan`,`amlservice`.`users`.`jkelamin` AS `jkelamin` from (((((`amlservice`.`wajihah` join `amlservice`.`users` on((`amlservice`.`wajihah`.`id` = `amlservice`.`users`.`wajihah_id`))) join `amlservice`.`jurusan` on((`amlservice`.`jurusan`.`id` = `amlservice`.`users`.`jurusan_id`))) join `amlservice`.`jabatan` on((`amlservice`.`jabatan`.`id` = `amlservice`.`users`.`jabatan_id`))) join `amlservice`.`fakultas` on((`amlservice`.`fakultas`.`id` = `amlservice`.`users`.`fakultas_id`))) join `amlservice`.`level` on((`amlservice`.`level`.`id` = `amlservice`.`users`.`level`))) where (`amlservice`.`users`.`level` = 3)
*/
