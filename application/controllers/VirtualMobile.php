<?php
class VirtualMobile extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view("virtual/amaliyah");
	}

	public function quran(){
		$this->load->view("virtual/quran");
	}

	public function hadits(){
		$this->load->view("virtual/hadits");
	}

	public function aktifitas(){
		$this->load->view("virtual/aktifitas");
	}

	public function mentor(){
		$this->load->view("virtual/mentor/kelas");
	}
	public function kader(){
		$this->load->view("virtual/mentor/kader");
	}
	public function tugas(){
		$this->load->view("virtual/mentor/tugas");
	}

	public function zikirpagi(){
		$this->load->view("virtual/zikirpagi");
	}

	public function zikirpetang(){
		$this->load->view("virtual/zikirpetang");
	}

}
