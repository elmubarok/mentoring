<?php
class Auth extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model("MAuth", "mauth");
	}

	function index(){
		if(!isset($_SESSION['uid'])){
			$this->load->view("auth");
		}else{
			// $this->goto($_SESSION['level']);
			redirect("Admin");
		}
	}

	function masuk(){
		$uname = $_POST['username'];
		$pass = $_POST['password'];
		/*
			separate user login
		 		1 => SuperAdmin, 2 => Admin, 3 => Mentor, 4 => Kader
		*/
		$result = $this->mauth->masuk($uname, $pass);

		if($result['stat']){
			$sess_dat_raw = $this->mauth->getforsession($result['id']);
			$sess_dat = array(
				"uid" => $sess_dat_raw->id,
				"uname" => $sess_dat_raw->username,
				"level" => $sess_dat_raw->level
			);
			// set session
			$this->session->set_userdata($sess_dat);
			/*
			mau di redirect k mana
			Superadmin => kelola admin -> 1
			Admin => kelola kader & mentor & kelas -> 2
			*/
			if($sess_dat['level'] == 1):
				redirect("admin");
			elseif($sess_dat['level'] == 2):
				redirect("admin");
			elseif($sess_dat['level'] == 3):
				redirect("VirtualMobile/mentor");
			else:
				redirect("auth");
			endif;
		}else{
			echo "false";
			redirect("auth");
		}
	}

	function keluar(){
		session_destroy();
		redirect("auth");
	}

}
