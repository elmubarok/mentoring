<?php
class Kaderisasi extends CI_Controller{

	public function __construct(){

		parent::__construct();
		//$this->load->model('kelas_model');
		$this->load->helper('form');
	}

	public function index(){
		if(isset($_SESSION['uid'])){
			$data['page_title'] = 'kelola Kaderisasi';
			$data['type'] = "8";

			$dbData['nama_kelas'] = $this->getClass();

			$this->load->view('header', $data);
			$this->load->view('kaderisasi/home', $dbData);
			$this->load->view('footer');
		}else{
			redirect("auth");
		}
	}

	private function getClass(){
		$this->db->select("nama_kelas, kelas_id");
		$this->db->group_by("nama_kelas");
		return $this->db->get("qr_grouping")->result();
	}

}
