<?php
class MAuth extends CI_Model{

	function masuk($uname, $pass){

		$this->db->select("id");
		$this->db->where("username", $uname);
		$this->db->where("password", $pass);

		if($this->db->get("users")->num_rows() > 0):
			$this->db->select("id");
			$this->db->where("username", $uname);
			$this->db->where("password", $pass);

			$id = $this->db->get("users")->row();
			return ["stat"=>true, "id"=>$id->id];
		else:
			return ["stat"=>false];
		endif;
	}

	function getforsession($id){
		/* return value
		id, username, level
		*/
		$this->db->select("id, username, level");
		$this->db->where("id", $id);
		return $this->db->get("users")->row();
	}

}
