<?php

function getMentoringByTglAll(){
    include "config.php";

    $sql = "SELECT * FROM qr_aktivitas_approved_by_tgl";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $conn = null;
        echo '{"data":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function destroy(){
    include "config.php";
    
    $sql = "TRUNCATE TABLE kuliah";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $conn = null;
        echo 1;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

if($_GET['action'] == 'getMentoringByTglAll'){
	getMentoringByTglAll();
}else if($_GET['action'] == 'destroy'){
    destroy();
}

?>
