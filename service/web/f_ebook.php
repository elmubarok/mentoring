<?php

function create(){
	include "config.php";
	$title = $conn->quote($_POST['title']);
  $content = $conn->quote($_POST['content']);
	$tgl = date('Y-m-d');

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "INSERT INTO ebook(title,content,tgl) VALUES ($title,$content,'$tgl')";
  $conn->exec($sql);

	$id = $conn->lastInsertId();
	$img = md5($conn->lastInsertId()).".jpg";

  $sqls = "UPDATE ebook SET image='$img' WHERE id='$id'";
  $conn->exec($sqls);

	echo json_encode(
		array(
			array("stat"=>"Success", "id"=>$id)
		)
	);
  $conn = null;
}

function update(){
	include "config.php";
	$id = $_POST['inpId'];
	$title = $conn->quote($_POST['title']);
  $content = $conn->quote($_POST['content']);

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "UPDATE ebook SET title = $title,
  	content = $content WHERE id = '$id'";
  $conn->exec($sql);
	echo "Success";
  $conn = null;
}

function getAllEbook(){
	include "config.php";
	$sql = "SELECT * FROM ebook ORDER BY id DESC";
	try {
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $conn->query($sql);
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);
			$conn = null;
			echo '{"data":'. json_encode($result) .'}';
	} catch(PDOException $e) {
			echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getEbookById($id){
    include 'config.php';
    $sql = "SELECT * FROM ebook WHERE id=:id";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(":id", $id);
        $stmt->execute();

        $result = $stmt->fetchObject();
        $dbh = null;
        echo '{"item":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function delete($id){
    include "config.php";
    $sql = "DELETE FROM ebook WHERE id='$id'";
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->exec($sql);
    $conn = null;
    echo json_encode(1);
}

function getDataById($id){
    include 'config.php';
    $sql = "SELECT * FROM ebook WHERE id=:id";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $result = $stmt->fetchObject();
        $dbh = null;
        echo '{"item":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

if($_GET['action'] == 'create'){
	create();
}
if($_GET['action'] == 'update'){
	update();
}
if($_GET['action'] == 'getAllEbook'){
	getAllEbook();
}
if($_GET['action'] == 'getEbookById'){
	getEbookById($_GET['id']);
}
if($_GET['action'] == 'delete'){
    $id = $_GET['id'];
    delete($id);
}
if($_GET['action'] == 'getDataById'){
    $id = $_GET['id'];
    getDataById($id);
}
?>
