<?php

function create(){
	include "config.php";
	$title = $_POST['inpTitle'];
	$content = $_POST['inpContent'];
	$tgl = date('Y-m-d');

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO pengumuman(title,content,tgl) values ('$title','$content','$tgl')";
    $conn->exec($sql);
		echo "Success";
    $conn = null;
}

function delete($id){
    include "config.php";
    
    $sql = "DELETE FROM pengumuman WHERE id='$id'";
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->exec($sql);

    $conn = null;

    echo json_encode(1);
}

function updatePengumuman(){
	include "config.php";
	$id = $_POST['inpId'];
	$title = $_POST['inpTitle'];
  $content = $_POST['inpContent'];

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE pengumuman SET title = '$title',
    	content = '$content' WHERE id = '$id'";
    $conn->exec($sql);
		echo "Success";
    $conn = null;
}

function getPengumumanAll(){
    include "config.php";
    $sql = "SELECT * FROM pengumuman ORDER BY id DESC";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $conn = null;
        echo '{"data":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getPengumumanById($id){
    include 'config.php';
    $sql = "SELECT * FROM pengumuman WHERE id=:id";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(":id", $id);
        $stmt->execute();

        $result = $stmt->fetchObject();
        $dbh = null;
        echo '{"item":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getMentorAll(){
    include "config.php";
    $sql = "SELECT * FROM qr_mentor";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $conn = null;
        echo '{"items":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

if($_GET['action'] == 'create'){
	create();
}
if($_GET['action'] == 'updatePengumuman'){
	updatePengumuman();
}
if($_GET['action'] == 'delete'){
	$id = $_GET['id'];
  delete($id);
}
if($_GET['action'] == 'getPengumumanAll'){
	getPengumumanAll();
}
if($_GET['action'] == 'getMentorAll'){
	getMentorAll();
}
if($_GET['action'] == 'getPengumumanById'){
	$id = $_GET['id'];
	getPengumumanById($id);
}

?>
