<?php
//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
//header("Access-Control-Allow-Headers: Content-Type, x-xsrf-token");
//------------
function create(){
	include "config.php";
	$username = $_POST['uname'];
	$password = $_POST['upswd'];
	$level = $_POST['level'];
	$wajihah_id = $_POST['ldkId'];
	$nama_panggilan = $_POST['callname'];
	$nama_lengkap = $_POST['fullname'];
	$hp = $_POST['hp'];
	$email = $_POST['email'];
	//$avatar = $_POST['inpMentorId'];
	$alamat_asal = $_POST['addrown'];
	$alamat_domisili = $_POST['addrdom'];
	$nim = $_POST['nim'];
	$jurusan_id = $_POST['jurusanId'];
	$fakultas_id = $_POST['fakultasId'];
	$angkatan = $_POST['angkatan'];
	$jabatan_id = $_POST['jabatanId'];
	$jkelamin = $_POST['jkelamin'];

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "INSERT INTO users(username,password,level,wajihah_id,
		nama_panggilan,nama_lengkap,hp,email,alamat_asal,alamat_domisili,
		nim,jurusan_id,fakultas_id,angkatan,jabatan_id,jkelamin)
		values ('$username','$password','$level','$wajihah_id',
			'$nama_panggilan','$nama_lengkap','$hp','$email','$alamat_asal','$alamat_domisili',
			'$nim','$jurusan_id','$fakultas_id','$angkatan','$jabatan_id','$jkelamin')";

	$conn->exec($sql);
	$conn = null;

	echo "Success";
}

function updateUser(){
	include "config.php";
	$id = $_POST['id'];
	$username = urldecode($_POST['uname']);
	$password = urldecode($_POST['upswd']);
	$level = $_POST['level'];
	$wajihah_id = $_POST['ldkId'];
	$nama_panggilan = urldecode($_POST['callname']);
	$nama_lengkap = urldecode($_POST['fullname']);
	$hp = $_POST['hp'];
	$email = urldecode($_POST['email']);
	//$avatar = $_POST['inpMentorId'];
	$alamat_asal = urldecode($_POST['addrown']);
	$alamat_domisili = urldecode($_POST['addrdom']);
	$nim = $_POST['nim'];
	$jurusan_id = $_POST['jurusanId'];
	$fakultas_id = $_POST['fakultasId'];
	$angkatan = $_POST['angkatan'];
	$jabatan_id = $_POST['jabatanId'];
	$jkelamin = $_POST['jkelamin'];
	//--

	try{
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "UPDATE users SET
		username = '$username',
		password = '$password',
		level = '$level',
		wajihah_id = '$wajihah_id',
		nama_panggilan = '$nama_panggilan',
		nama_lengkap = '$nama_lengkap',
		hp = '$hp',
		email = '$email',
		alamat_asal = '$alamat_asal',
		alamat_domisili = '$alamat_domisili',
		nim = '$nim',
		jurusan_id = '$jurusan_id',
		fakultas_id = '$fakultas_id',
		angkatan = '$angkatan',
		jabatan_id = '$jabatan_id',
		jkelamin = '$jkelamin' WHERE id = '$id'";
		//$sql = "update datateman set nama='$nama',alamat='$alamat',spesialis='$spesialis',fb='$fb',icon='$icon' where id ='$id'";
		$conn->exec($sql);
		$conn = null;
		echo "Success";
	}catch(Exception $e){
		echo $e;
	}
}

function delete($id){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "DELETE FROM users WHERE id='$id'";

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->exec($sql);

	$conn = null;

	echo json_encode(1);
}

// for superadmin
function getSUserAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM qr_superadmin ORDER BY id DESC";
	try {

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"data":'. json_encode($result) .'}';


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getUserAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM qr_admin ORDER BY wajihah_id";
	try {

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"data":'. json_encode($result) .'}';


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getJabatanAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM jabatan";
	try {
		//$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"items":'. json_encode($result) .'}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

function getFakultasAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM fakultas";
	try {
		//$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"items":'. json_encode($result) .'}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

function getJurusanAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM jurusan";
	try {
		//$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"items":'. json_encode($result) .'}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

function getLdkAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM wajihah";
	try {
		//$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"items":'. json_encode($result) .'}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

function getLevelAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM level";
	try {
		//$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"items":'. json_encode($result) .'}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

// for superadmin
function getSUserById($id){

	include 'config.php';
	$sql = "SELECT * FROM qr_superadmin WHERE id=:id";

	try {

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->prepare($sql);

		$stmt->bindParam(":id", $id);
		$stmt->execute();

		$result = $stmt->fetchObject();
		$dbh = null;
		echo '{"item":'. json_encode($result) .'}';


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

function getUserById($id){

	include 'config.php';
	$sql = "SELECT * FROM qr_admin WHERE id=:id";

	try {

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->prepare($sql);

		$stmt->bindParam(":id", $id);
		$stmt->execute();

		$result = $stmt->fetchObject();
		$dbh = null;
		echo '{"item":'. json_encode($result) .'}';


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

$action = $_GET['action'];

switch ($action) {
	case 'create':
		create();
		break;
	case 'delete':
		$id = $_GET['id'];
		delete($id);
		break;
	case 'updateUser':
		updateUser();
		break;
	case 'getSUserAll':
		getSUserAll();
		break;
	case 'getUserAll':
		getUserAll();
		break;
	case 'getJabatanAll':
		getJabatanAll();
		break;
	case 'getFakultasAll':
		getFakultasAll();
		break;
	case 'getJurusanAll':
		getJurusanAll();
		break;
	case 'getLdkAll':
		getLdkAll();
		break;
	case 'getLevelAll':
		getLevelAll();
		break;
	case 'getSUserById':
		$id = $_GET['id'];
		getSUserById($id);
		break;
	case 'getUserById':
		$id = $_GET['id'];
		getUserById($id);
		break;
}

?>
