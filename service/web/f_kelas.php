<?php

function create(){
	include "config.php";

	$nama = $_POST['inpKelasName'];
	$lokasi = $_POST['inpLokasi'];
	$hari = $_POST['inpHari'];
	$jam = $_POST['inpJam'];
	$linkwa = $_POST['inpLinkWa'];
	$mentor_id = $_POST['inpMentorId'];
	$jkelamin = $_POST['jkelamin'];

	try{
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    $sql = "INSERT INTO
							kelas (mentor_id,nama,hari,waktu,tempat,link_group_wa, kelas_jkelamin)
							values ('$mentor_id','$nama','$hari','$jam','$lokasi','$linkwa', '$jkelamin')";

	    $conn->exec($sql);
	    $conn = null;
			echo "Success";
	}catch(PDOException $e){
		echo 'Connection failed: ' . $e->getMessage();
	}
}

function updateKelas(){
	include "config.php";

	$id = $_POST['inpId'];
	$nama = $_POST['inpKelasName'];
	$lokasi = $_POST['inpLokasi'];
	$hari = $_POST['inpHari'];
	$jam = $_POST['inpJam'];
	$linkwa = $_POST['inpLinkWa'];
	$mentor_id = $_POST['inpMentorId'];
	$jkelamin = $_POST['jkelamin'];

	try{
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "UPDATE kelas SET
		mentor_id ='$mentor_id',
		nama = '$nama',
		hari = '$hari',
		waktu = '$jam',
		tempat = '$lokasi',
		link_group_wa = '$linkwa',
		kelas_jkelamin = '$jkelamin' WHERE id = '$id'";
		//$sql = "update datateman set nama='$nama',alamat='$alamat',spesialis='$spesialis',fb='$fb',icon='$icon' where id ='$id'";
		$conn->exec($sql);
		$conn = null;
		echo "Success";
	}catch(Exception $e){
		echo $e;
	}
}

function getKelasAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM qr_kelas ORDER BY id DESC";
	try {

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"data":'. json_encode($result) .'}';


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getMentorAll(){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "SELECT * FROM qr_mentor";
	try {

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$conn = null;
		echo '{"items":'. json_encode($result) .'}';


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getKelasById($id){
    include 'config.php';
    $sql = "SELECT * FROM qr_kelas WHERE id=:id";

    try {

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(":id", $id);
        $stmt->execute();

        $result = $stmt->fetchObject();
        $dbh = null;
        echo '{"item":'. json_encode($result) .'}';


    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function delete($id){
	// variabel koneksi
	include "config.php";

	// query untuk menampilkan data
	$sql = "DELETE FROM kelas WHERE id='$id'";

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->exec($sql);

	$conn = null;

	echo json_encode(1);
}

$action = $_GET['action'];

switch ($action) {
	case 'create':
		create();
		break;
	case 'updateKelas':
		updateKelas();
		break;
	case 'getKelasAll':
		getKelasAll();
		break;
	case 'getMentorAll':
		getMentorAll();
		break;
	case 'getKelasById':
		$id = $_GET['id'];
		getKelasById($id);
		break;
	case 'delete':
		$id = $_GET['id'];
		delete($id);
		break;
}

?>
