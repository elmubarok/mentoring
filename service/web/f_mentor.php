<?php

//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
//header("Access-Control-Allow-Headers: Content-Type, x-xsrf-token");
//------------

function create(){
    include "config.php";

    $username = $_POST['uname'];
    $password = $_POST['upswd'];
    $level = $_POST['level'];
    $nama_panggilan = $_POST['callname'];
    $nama_lengkap = $_POST['fullname'];
    $hp = $_POST['hp'];
    $email = $_POST['email'];

    //$avatar = $_POST['inpMentorId'];

    $alamat_asal = $_POST['addrown'];
    $alamat_domisili = $_POST['addrdom'];
    $jkelamin = $_POST['jkelamin'];
    //--

    try{
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "INSERT INTO users(username,password,level,".
      "nama_panggilan,nama_lengkap,hp,email,alamat_asal,alamat_domisili,".
      "jkelamin) ".
      "values ('$username','$password','$level',".
      "'$nama_panggilan','$nama_lengkap','$hp','$email','$alamat_asal','$alamat_domisili',".
      "'$jkelamin')";

      $conn->exec($sql);

      echo "Success";
    }catch(PDOException $e){
      echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

    $conn = null;
}

function updateUser(){
    include "config.php";
    $id = $_POST['id'];
    $username = $_POST['uname'];
    $password = $_POST['upswd'];
    $level = $_POST['level'];

    $nama_panggilan = $_POST['callname'];
    $nama_lengkap = $_POST['fullname'];
    $hp = $_POST['hp'];
    $email = $_POST['email'];

    $alamat_asal = $_POST['addrown'];
    $alamat_domisili = $_POST['addrdom'];

    $jkelamin = $_POST['jkelamin'];
    //--

    try {
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "UPDATE users SET
          username = '$username',
          password = '$password',
          level = '$level',
          nama_panggilan = '$nama_panggilan',
          nama_lengkap = '$nama_lengkap',
          hp = '$hp',
          email = '$email',
          alamat_asal = '$alamat_asal',
          alamat_domisili = '$alamat_domisili',
          jkelamin = '$jkelamin' WHERE id = '$id'";

      $conn->exec($sql);
      echo "Success";
    }catch(PDOException $e){
      echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

    $conn = null;
}

function getUserAll(){
    // variabel koneksi
    include "config.php";

    // query untuk menampilkan data
    $sql = "SELECT * FROM qr_mentor";
    try {

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $conn = null;
            echo '{"data":'. json_encode($result) .'}';


    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function delete($id){
    // variabel koneksi
    include "config.php";

    // query untuk menampilkan data
    $sql = "DELETE FROM users WHERE id='$id'";

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->exec($sql);

    $conn = null;

    echo json_encode(1);
}

function getUserById($id){

    include 'config.php';
    $sql = "SELECT * FROM qr_mentor WHERE id=:id";

    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(":id", $id);
        $stmt->execute();

        $result = $stmt->fetchObject();
        $dbh = null;
        echo '{"item":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}

$action = $_GET['action'];

switch ($action) {
  case 'create':
    create();
    break;
  case 'delete':
    $id = $_GET['id'];
    delete($id);
    break;
  case 'updateUser':
    updateUser();
    break;
  case 'getUserAll':
    getUserAll();
    break;
  case 'getUserById':
    $id = $_GET['id'];
    getUserById($id);
    break;
}

?>
