<?php

function getMentoringByMyAll(){
    include "config.php";

    // $bulan=$_GET['bulan'];
    // $tahun=$_GET['tahun'];

    // $sql = "SELECT * FROM qr_aktivitas_approved_by_my WHERE bulan='$bulan' AND tahun='$tahun' ";
    $sql = "SELECT * FROM qr_aktivitas_approved_by_my";

    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $conn = null;
        echo '{"data":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

if($_GET['action'] == 'getMentoringByMyAll'){
	getMentoringByMyAll();
}

?>
