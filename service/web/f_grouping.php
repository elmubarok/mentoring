<?php

function create(){
	include "config.php";
	$kelas_id = $_POST['kelasId'];
	$kader_id = $_POST['kaderId'];

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO kelas_kader(kelas_id,kader_id) values ('$kelas_id','$kader_id')";
    $conn->exec($sql);
		echo "Success";
    $conn = null;
}

function del(){
	include "config.php";
	$id = $_GET['id'];

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "DELETE FROM kelas_kader WHERE id='$id'";
    $conn->exec($sql);
		echo "1";
    $conn = null;
}

function createBatch($data, $trunct){
	include "config.php";

	try {
		if($trunct):
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sqlTrunc = "TRUNCATE TABLE kelas_kader";
		  $conn->exec($sqlTrunc);
			$connTrunc = null;
		endif;

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO kelas_kader(kelas_id,kader_id)".$data;
	  $conn->exec($sql);
	  $conn = null;
		return true;
	} catch(PDOException $e) {
		return false;
	}
}

function updateGrouping(){
	include "config.php";
	$kelas_id = $_POST['kelasId'];
  $kader_id = $_POST['kaderId'];
	$id = $_POST['inpId'];
	//--
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE kelas_kader SET kelas_id = '$kelas_id',
    	kader_id = '$kader_id' WHERE id = '$id'";

    $conn->exec($sql);
		echo "Success";
    $conn = null;
}

function getGroupingAll(){
    include "config.php";
    // query untuk menampilkan data
    $sql = "SELECT * FROM qr_grouping";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $conn = null;
            echo '{"data":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getKelasAll(){
    include "config.php";
    // query untuk menampilkan data
    $sql = "SELECT id, nama_kelas as nama FROM qr_kelas";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $conn = null;
            echo '{"items":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getKelasJk($type){
    include "config.php";
    // query untuk menampilkan data
    $sql = "SELECT id FROM qr_kelas WHERE jkelamin = '$type'";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;

				$res = array();
				// return as 1D array
				for($i=0; $i<count($result); $i++){
					array_push($res, $result[$i]['id']);
				}
				return $res;

        // echo '{"items":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getKelasJkArr($type){
    include "config.php";
    // query untuk menampilkan data
    $sql = "SELECT id, nama_kelas FROM qr_kelas WHERE jkelamin = '$type'";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;

        echo '{"items":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getKaderAll(){
    include "config.php";
    // query untuk menampilkan data
    $sql = "SELECT id, nama_lengkap as nama FROM qr_kader";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $conn = null;
        echo '{"items":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getKaderJk($type){
    include "config.php";
    // query untuk menampilkan data
    $sql = "SELECT id FROM qr_kader WHERE jkelamin = '$type'";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;
				$res = array();
				// return as 1D array
				for($i=0; $i<count($result); $i++){
					array_push($res, $result[$i]['id']);
				}
				return $res;
        // echo '{"items":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getGroupingById($id){
    include 'config.php';
    $sql = "SELECT * FROM qr_grouping WHERE grouping_id=:id";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(":id", $id);
        $stmt->execute();

        $result = $stmt->fetchObject();
        $dbh = null;
        echo '{"item":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getGroupingByType($type){
    include 'config.php';
    $sql = "SELECT * FROM qr_grouping WHERE jkelamin=:type";
    try {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(":type", $type);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $dbh = null;
        echo '{"data":'. json_encode($result) .'}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function randomKelas(){

	$laki = getKaderJk("l"); $perempuan = getKaderJk("p");
	$klaki = getKelasJk("l"); $kperempuan = getKelasJk("p");

	// random array
	for($i=0; $i<3; $i++){
		shuffle($laki);
		shuffle($perempuan);
	}

	// insert to datatabase
	if(createBatch(getFinalRand($klaki, $laki), true) &&
		createBatch(getFinalRand($kperempuan, $perempuan), false)):
		echo "Success";
	else:
		echo "Failed";
	endif;

	// $finalKlaki = array();
	// $sql = "VALUES ('0','0')";

	// echo "<br><br>";
	// if(count($laki) % count($klaki) == 0){
		// $iter = count($laki)/count($klaki);/

		// $cntLaki = count($laki);
		// $mod = count($laki)%count($klaki);
		// $iter = (count($laki)-$mod)/count($klaki);
		//
		// echo "<br><br>";
		// echo count($laki)%count($klaki);
		// echo round((count($laki)-$mod)/count($klaki));

		// for($j=0; $j<count($klaki); $j++){
		// 	$pos = $j * $iter;
		// 	for($i=0; $i<$iter; $i++){
		// 		$finalKlaki[$j][$i] = $laki[$pos+$i];
		// 	}
		// }
		//
		// $modArr = array_slice($laki, -$mod, $mod);
		// for($i=0; $i<count($modArr); $i++){
		// 	array_push($finalKlaki[$i], $modArr[$i]);
		// }
		//
		// print_r($finalKlaki);

		// pass to database
		// for($kls=0; $kls<count($klaki); $kls++){
		// 	for($f=0; $f<count($finalKlaki[$kls]); $f++){
		// 		echo $finalKlaki[$kls][$f]." ";
		// 		$cur = $finalKlaki[$kls][$f];
		// 		// $sql .= ",('$klaki[$kls]', '$cur')";
		// 	}
		// 	echo "<br><br>";
		// }

		// echo $sql;
		// createBatch($sql);

		// print_r($finalKlaki);
	// }
}

function getFinalRand($arrKls, $arrPrsn){
	$finalKls = array();
	$sql = "VALUES ('0','0')";

	if(count($arrPrsn) % count($arrKls) == 0){
		$iter = count($arrPrsn)/count($arrKls);
		for($j=0; $j<count($arrKls); $j++){
			$pos = $j * $iter;
			for($i=0; $i<$iter; $i++){
				$finalKls[$j][$i] = $arrPrsn[$pos+$i];
			}
		}
		// pass to database
		for($kls=0; $kls<count($arrKls); $kls++){
			for($f=0; $f<count($finalKls[$kls]); $f++){
				$cur = $finalKls[$kls][$f];
				$sql .= ",('$arrKls[$kls]', '$cur')";
			}
		}
		return $sql;

	}else{
		$cntLaki = count($arrPrsn);
		$mod = count($arrPrsn)%count($arrKls);
		$iter = (count($arrPrsn)-$mod)/count($arrKls);

		for($j=0; $j<count($arrKls); $j++){
			$pos = $j * $iter;
			for($i=0; $i<$iter; $i++){
				$finalKls[$j][$i] = $arrPrsn[$pos+$i];
			}
		}

		$modArr = array_slice($arrPrsn, -$mod, $mod);
		for($i=0; $i<count($modArr); $i++){
			array_push($finalKls[$i], $modArr[$i]);
		}

		for($kls=0; $kls<count($arrKls); $kls++){
			for($f=0; $f<count($finalKls[$kls]); $f++){
				$cur = $finalKls[$kls][$f];
				$sql .= ",('$arrKls[$kls]', '$cur')";
			}
		}
		return $sql;

	}

}

/*
query for get opposite of join
---
SELECT users.id, users.jkelamin, users.nama_panggilan FROM users
LEFT JOIN kelas_kader
ON users.id = kelas_kader.kader_id
WHERE users.level = "4" 
AND kelas_kader.kader_id IS NULL

query for get 
*/

function renewKelas(){
	$unbindkader = getUnbindKader();
	// print_r($unbindkader);
	// echo "-----";

	for($i=0; $i<count($unbindkader); $i++){

		renew($unbindkader[$i]['id'],
					$unbindkader[$i]['jkelamin']);

	}

	echo "done";
}
# get kader yang belum masuk ke dalam kelas
function getUnbindKader(){
	include "config.php";
	$sql = "SELECT users.id, users.jkelamin, users.nama_panggilan FROM users
			LEFT JOIN kelas_kader
			ON users.id = kelas_kader.kader_id
			WHERE users.level = '4' 
			AND kelas_kader.kader_id IS NULL";
	try{
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;
        return $result;
	}catch(PDOException $e){

	}
}
# get jumlah kelas yang sudah terisi beserta
# jumlah peserta / kadernya
function getBindKelas($jenis){
	include "config.php";
	$sql = "SELECT
			kelas.id AS kelas_id, COUNT(kelas_kader.kelas_id) AS jml_kader, 
			kelas.kelas_jkelamin AS jkelamin
			FROM kelas, kelas_kader
			WHERE kelas.id = kelas_kader.kelas_id
			AND kelas.kelas_jkelamin = '$jenis'
			GROUP BY kelas_kader.kelas_id";
	try{
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$query = $conn->query($sql);
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		$conn = null;
		return $result;
	}catch(PDOException $err){

	}
}
# memulai update database
function renew($kader_id, $jenis){
	include "config.php";
	$bindkelas = getBindKelas($jenis);
	$minjml = min(array_column($bindkelas, 'jml_kader'));
	$minres = array_values(array_filter($bindkelas,
				function($minres) use ($minjml){ return $minjml == $minres['jml_kader']; }));
	// print_r($minres);
	$kelas_id = $minres[0]['kelas_id'];

	try{
		$sql = "INSERT INTO 
				kelas_kader(kelas_id, kader_id) 
				VALUES 
				('$kelas_id','$kader_id')";
		$conn->exec($sql);
		$conn = null;
	}catch(PDOException $err){
		echo $err;
	}

}

# cek kader baru
function checkNewKader(){
	include "config.php";
	$sql = "SELECT COUNT(users.id) AS jml_kader FROM users
			LEFT JOIN kelas_kader
			ON users.id = kelas_kader.kader_id
			WHERE users.level = '4' 
			AND kelas_kader.kader_id IS NULL";
	try{
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;
        echo $result[0]['jml_kader'];
	}catch(PDOException $e){

	}
}

$action = $_GET['action'];

switch ($action) {
	// for debug
	case 'isiKelas':
		randomKelas();
	break;

	case 'renew':
		renewKelas();
	break;
	case 'checknew':
		checkNewKader();
	break;

	case 'create':
		create();
		break;
	case 'delete':
		del();
		break;
	case 'updateGrouping':
		updateGrouping();
		break;
	case 'getKelasAll':
		getKelasAll();
		break;
	case 'getKaderAll':
		getKaderAll();
		break;
	case 'getGroupingAll':
	  getGroupingAll();
		break;
	case 'getGroupingById':
		$id = $_GET['id'];
		getGroupingById($id);
		break;
	case 'getGroupingByType':
		$type = $_GET['type'];
		getGroupingByType($type);
		break;
	case 'getKelasType':
		$type = $_GET['type'];
		getKelasJkArr($type);
		break;
}

?>
