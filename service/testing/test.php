<?php

	include 'config.php';

	function db(){
		$conn = new Config();
		return $db = $conn->openConnection();
	}

	function generate_test($phase){

		$db = db();

		$sql = "INSERT INTO users(username,password,level,wajihah_id,
		nama_panggilan,nama_lengkap,hp,email,alamat_asal,alamat_domisili,
		nim,jurusan_id,fakultas_id,angkatan,jabatan_id,jkelamin) values ";

		$username = $_POST['username'];
		$password = $_POST['password'];
		$level = $_POST['level'];
		$wajihah_id = $_POST['wajihah_id'];
		$nama_panggilan = $_POST['nama_panggilan'];
		$nama_lengkap = $_POST['nama_lengkap'];
		$hp = $_POST['hp'];
		$email = $_POST['email'];
		$alamat_asal = $_POST['alamat_asal'];
		$alamat_domisili = $_POST['alamat_domisili'];
		$nim = $_POST['nim'];
		$jurusan_id = $_POST['jurusan_id'];
		$fakultas_id = $_POST['fakultas_id'];
		$angkatan = $_POST['angkatan'];
		$jabatan_id = $_POST['jabatan_id'];
		$jkelamin = $_POST['jkelamin'];

		for($i=0; $i<$phase; $i++):
			$sql .= "('$username','$password','$level','$wajihah_id',
		'$nama_panggilan','$nama_lengkap','$hp','$email','$alamat_asal','$alamat_domisili',
		'$nim','$jurusan_id','$fakultas_id','$angkatan','$jabatan_id','$jkelamin')";
			($i == $phase-1) ? '' : $sql.=',';
		endfor;

		try{

			if($db->exec($sql)):
				echo json_encode(array("status"=>"true", "msg"=>"insert success"));
			endif;

			$db = null;

		}catch(PDOExeption $err){
			echo json_encode(array("status"=>"false", "msg"=>$err));
		}
	}

	generate_test($_POST['total']);

	// $sql .= "('superadmin','1','1','Rusdi',
	// 		'Rusdi Hartanto','0857412589639','rusdi@gmail.com','avatar1.jpg','jl. danau toba indah 88, jakarta','jl. Bromo Indah 78, malang',
	// 		'879589','3','2','2017','1','Laki-laki')";