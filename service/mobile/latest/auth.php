<?php

include_once 'config.php';

class Auth{
  private $conn;
  private $db;

  function __construct(){
    $this->conn = new Config();
    $this->db = $this->conn->openConnection();
  }

  function check(){
    return
      $this->db->getAttribute(
        PDO::ATTR_CONNECTION_STATUS);
  }

  function getAuth(){
    try{
      $statement = $this->db->prepare(
        "SELECT users.*, fakultas.nama as fakultas, 
        jurusan.nama as jurusan, jabatan.nama as jabatan,
        wajihah.nama as wajihah 
        FROM users, fakultas, jurusan, jabatan, wajihah
        WHERE
        users.fakultas_id = fakultas.id AND
        users.jurusan_id = jurusan.id AND
        users.jabatan_id = jabatan.id AND
        users.wajihah_id = wajihah.id AND
        users.username = :uname AND 
        users.password = :pass"
      );

      $statement->bindParam(':uname', $username);
      $statement->bindParam(':pass', $password);
      $username = $_POST['uname'];
      $password = $_POST['upass'];
      $statement->execute();
      $data = $statement->fetchAll();

      if(count($data) > 0){
        // echo json_encode([["stat"=>"true"],["data"=>$data]]);
        $data[0] += array("stat"=>"true");
        echo json_encode($data);
      }else{
        echo json_encode([["stat"=>"false"]]);
      }
    }catch(PDOException $err){
      echo json_encode([["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

}

$auth = new Auth();
$auth->getAuth();

?>
