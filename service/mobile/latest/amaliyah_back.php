<?php

include_once 'config.php';

class Amaliyah{
  private $conn;
  private $db;

  function __construct(){
    $this->conn = new Config();
    $this->db = $this->conn->openConnection();
  }

  function check(){
    return
      $this->db->getAttribute(
        PDO::ATTR_CONNECTION_STATUS);
  }

  // insert to materi
  // needed (POST method):
  // - kader_id
  // - materi_id
  // - tgl
  // - tahun
  // - bulan
  // $which mean amal/surat/hadits
  // format msg : '$which'='materi_id'
  // ex : amal=23&amal=24&amal=25

  // return id, nama
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function insert(){
  	$which = $_POST['which'];
    try{
      $statement = $this->db->prepare(
        "INSERT INTO kuliah
        (kader_id, materi_id, tgl, tahun, bulan, is_approved)
        VALUES
        (:kader_id, :materi_id, :tgl, :tahun, :bulan, :is_approved)"
      );

      $statement->bindParam(':kader_id', $kader_id);
      $statement->bindParam(':materi_id', $materi_id);
      $statement->bindParam(':tgl', $tgl);
      $statement->bindParam(':tahun', $tahun);
      $statement->bindParam(':bulan', $bulan);
      $statement->bindParam(':is_approved', $is_approved);

      if(isset($_POST[$which])){
        $data = $_POST[$which];
        for($i=0; $i<count($data); $i++):
          $kader_id = $_POST['kader_id'];
          $materi_id = $data[$i];
          $tgl = date("Y-m-d");
          $tahun = date("Y");
          $bulan = date("m");
          $is_approved = ($which == "amal") ? "1" : "0";
          $statement->execute();
        endfor;
      }else{
        echo json_encode([["stat"=>"null"]]);
        return false;
      }

      echo json_encode([["stat"=>"true"]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>"false"]]);
    }
    $this->conn->closeConnection();
  }

  // get all surat
  // return id, nama
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function getSurah(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, nama FROM `materi` WHERE is_surat = 1"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }

  // get all hadits
  // return id, nama
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function getHadits(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, nama FROM `materi` WHERE is_hadist = 1"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }
  
  function getAmal(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, nama FROM `materi` WHERE is_amal = 1"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }

  // get tgl
  // needed (GET method):
  // - kader_id
  // return encoded json:
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function getAktifitasTgl(){
    try{
      $statement = $this->db->prepare(
        "SELECT tgl FROM qr_rekap_aktivitas_by_tgl
         WHERE kader_id = :kader
         GROUP BY tgl"
      );
      $statement->bindParam(':kader', $kader);
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }

  // aktifitas saya
  // needed (GET method):
  // - kader_id
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function getAktifitasByTgl(){
    try{
      $statement = $this->db->prepare(
        "SELECT * FROM qr_rekap_aktivitas_by_tgl
        WHERE kader_id = :kader"
      );
      $statement->bindParam(':kader', $kader);
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }

  // aktifitas filter by tanggal
  // needed (GET method):
  // - tgl
  // - kader_id
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function getAktifitasByTglOne(){
    try{
      $statement = $this->db->prepare(
        "SELECT * FROM qr_rekap_aktivitas_by_tgl
        WHERE tgl = :tgl AND kader_id = :kader"
      );

      $statement->bindParam(':tgl', $tgl);
      $statement->bindParam(':kader', $kader);
      $tgl = $_GET['tgl'];
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }

  // get detail per item
  // needed (GET method) :
  // - tipe
  // - tgl
  // - kader_id
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  function getAktifitasByMateri(){
    try{
      $sql =
        "SELECT materi.nama AS nama_materi, qr_rekap_aktivitas_by_tglmateri.*
        FROM
        qr_rekap_aktivitas_by_tglmateri, materi
        WHERE
        qr_rekap_aktivitas_by_tglmateri.materi_id = materi.id AND qr_rekap_aktivitas_by_tglmateri.tgl = :tgl
        AND qr_rekap_aktivitas_by_tglmateri.".$_GET['tipe']." = '1'
        AND qr_rekap_aktivitas_by_tglmateri.kader_id = :kader";
      $statement = $this->db->prepare($sql);

      $statement->bindParam(':tgl', $tgl);
      $statement->bindParam(':kader', $kader);
      $tgl = $_GET['tgl'];
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>$err->getMessage()]]);
    }
  }
}

$amaliyah = new Amaliyah();
$mode = $_GET['mode'];

switch ($mode) {
  // insert to materi
  // needed (POST method):
  // - kader_id
  // - materi_id
  // - tgl
  // - tahun
  // - bulan
  // $which mean amal/surat/hadits
  // format msg : '$which'='materi_id'
  // ex : amal=23&amal=24&amal=25

  // return id, nama
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'insert':
    //$which = $_POST['which'];
    $amaliyah->insert();
  break;

  // get all surat
  // return id, nama
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'surah':
    $amaliyah->getSurah();
  break;

  // get all hadits
  // return id, nama
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'hadits':
    $amaliyah->getHadits();
  break;
  
  case 'amal':
    $amaliyah->getAmal();
  break;

  // aktifitas saya
  // needed (GET method):
  // - kader_id
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'rekap_tgl':
    $amaliyah->getAktifitasByTgl();
  break;

  // aktifitas filter by tanggal
  // needed (GET method):
  // - tgl
  // - kader_id
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'rekap_tgl_one':
    $amaliyah->getAktifitasByTglOne();
  break;

  // get tgl
  // needed (GET method):
  // - kader_id
  // return encoded json:
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'get_aktifitas_tgl':
    $amaliyah->getAktifitasTgl();
  break;

  // get detail per item
  // needed (GET method) :
  // - tipe
  // - tgl
  // - kader_id
  // [{stat: 'true'},{data: [{..:..},{..:..},..]}]
  // [{stat: err_msg}]
  case 'get_aktifitas_materi':
    $amaliyah->getAktifitasByMateri();
  break;
}

?>
