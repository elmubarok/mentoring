<?php

include_once 'config.php';

function db(){
	$conn = new Config();
	$db = $conn->openConnection();
	return $db;
}

function rvImage(){
	$title = $_POST["title"];
	$img = $_POST["img"];

	$upPath = $_SERVER["DOCUMENT_ROOT"]."/Mentoring/assets/img/";

	doUpload($title, $img, $upPath);
}

# it will upload image to server directory
function doUpload($title, $img, $path){
	if(
		file_put_contents($path.$title.".jpg", base64_decode($img))){
		updateDb($title);
	}else{
		echo json_encode(array("stat" => "false"));
	}
}

# update informations to db when successfully upload the image
function updateDb($title){
	$db = db();
	try{
		$id = $_POST["kader_id"];
		$sql = "UPDATE users SET avatar = '".$title.".jpg' WHERE id = $id";

		$db->exec($sql);
		$conn = null;

		echo json_encode(array("stat" => "true"));
	}catch(PDOException $err){
		echo json_encode(array("stat" => "false"));
	}

}

# direct call this function
rvImage();