<?php

include_once 'config.php';

class Amaliyah{
  private $conn;
  private $db;

  function __construct(){
    $this->conn = new Config();
    $this->db = $this->conn->openConnection();
  }

  function check(){
    return
      $this->db->getAttribute(
        PDO::ATTR_CONNECTION_STATUS);
  }

  function insert(){
  	$which = $_POST['which'];
    try{
      $statement = $this->db->prepare(
        "INSERT INTO kuliah
        (kader_id, materi_id, tgl, tahun, bulan, is_approved)
        VALUES
        (:kader_id, :materi_id, :tgl, :tahun, :bulan, :is_approved)"
      );

      $statement->bindParam(':kader_id', $kader_id);
      $statement->bindParam(':materi_id', $materi_id);
      $statement->bindParam(':tgl', $tgl);
      $statement->bindParam(':tahun', $tahun);
      $statement->bindParam(':bulan', $bulan);
      $statement->bindParam(':is_approved', $is_approved);

      if(isset($_POST[$which])){
        $data = $_POST[$which];
        for($i=0; $i<count($data); $i++):
          $kader_id = $_POST['kader_id'];
          $materi_id = $data[$i];
          $tgl = date("Y-m-d");
          $tahun = date("Y");
          $bulan = date("m");
          $is_approved = ($which == "amal") ? "1" : "0";
          $statement->execute();
        endfor;
      }else{
        echo json_encode([["stat"=>"null"]]);
        return false;
      }

      echo json_encode([["stat"=>"true"]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
    $this->conn->closeConnection();
  }

  function getSurah(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, nama FROM `materi` WHERE is_surat = 1"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getHadits(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, nama FROM `materi` WHERE is_hadist = 1"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getAmal(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, nama FROM `materi` WHERE is_amal = 1"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getAktifitasTgl(){
    try{
      $statement = $this->db->prepare(
        "SELECT tgl FROM qr_rekap_aktivitas_by_tgl
         WHERE kader_id = :kader
         GROUP BY tgl"
      );
      $statement->bindParam(':kader', $kader);
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getAktifitasByTgl(){
    try{
      $statement = $this->db->prepare(
        "SELECT * FROM qr_rekap_aktivitas_by_tgl
        WHERE kader_id = :kader"
      );
      $statement->bindParam(':kader', $kader);
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode($data);

      // echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo $err;
      // echo json_encode(
        // [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getAktifitasByTglOne(){
    try{
      $statement = $this->db->prepare(
        "SELECT * FROM qr_rekap_aktivitas_by_tgl
        WHERE tgl = :tgl AND kader_id = :kader"
      );

      $statement->bindParam(':tgl', $tgl);
      $statement->bindParam(':kader', $kader);
      $tgl = $_GET['tgl'];
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getAktifitasByMateri(){
    try{
      $sql =
        "SELECT materi.nama AS nama_materi, qr_rekap_aktivitas_by_tglmateri.*
        FROM
        qr_rekap_aktivitas_by_tglmateri, materi
        WHERE
        qr_rekap_aktivitas_by_tglmateri.materi_id = materi.id AND qr_rekap_aktivitas_by_tglmateri.tgl = :tgl
        AND qr_rekap_aktivitas_by_tglmateri.".$_GET['tipe']." = '1'
        AND qr_rekap_aktivitas_by_tglmateri.kader_id = :kader";
      $statement = $this->db->prepare($sql);

      $statement->bindParam(':tgl', $tgl);
      $statement->bindParam(':kader', $kader);
      $tgl = $_GET['tgl'];
      $kader = $_GET['kader_id'];
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getZikirPP(){
    try{
      $sql = 'SELECT id,title FROM dzikir_pagi_petang';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

      header('Content-Type: text/html; charset=utf-8');
      echo json_encode(
        [["stat"=>"true"],["data"=>$result]],
        JSON_UNESCAPED_UNICODE);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }
  function getZikirPPDetail(){
    try{
      $id = $_GET['id'];

      $sql = 'SELECT id,content FROM dzikir_pagi_petang where id = "' . $id. '"';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

      header('Content-Type: text/html; charset=utf-8');
      echo json_encode(
        [["stat"=>"true"],["data"=>$result]],
        JSON_UNESCAPED_UNICODE);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getZikirSholat(){
    try{
      $sql = 'SELECT id,title FROM dzikir_setelah_shalat';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

      header('Content-Type: text/html; charset=utf-8');
      echo json_encode(
        [["stat"=>"true"],["data"=>$result]],
        JSON_UNESCAPED_UNICODE);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }
  function getZikirSholatDetail(){
    try{
      $id = $_GET['id'];

      if(isset($_GET['id'])):
        $sql = 'SELECT * FROM dzikir_setelah_shalat WHERE id = ' . $id;
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        header('Content-Type: text/html; charset=utf-8');
        echo json_encode(
          [["stat"=>"true"],["data"=>$result]],
          JSON_UNESCAPED_UNICODE);
      else:
        echo json_encode(
          [["stat"=>"false"],["data"=>"This table need id parameter"]],
          JSON_UNESCAPED_UNICODE);
      endif;
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getListEbook(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, title FROM ebook ORDER BY id DESC"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }
  function getEbook(){
    try{
      $id = $_GET['id'];

      if(isset($_GET['id'])):
        $sql = 'SELECT * FROM ebook WHERE id = ' . $id;
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        header('Content-Type: text/html; charset=utf-8');
        echo json_encode(
          [["stat"=>"true"],["data"=>$result]],
          JSON_UNESCAPED_UNICODE);
      else:
        echo json_encode(
          [["stat"=>"false"],["data"=>"This table need id parameter"]],
          JSON_UNESCAPED_UNICODE);
      endif;
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getListDoa(){
    try{
      $statement = $this->db->prepare(
        "SELECT id, title FROM doa_pilihan ORDER BY id DESC"
      );
      $statement->execute();
      $data = $statement->fetchAll();

      echo json_encode([["stat"=>"true"],["data"=>$data]]);
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }
  function getDoa(){
    try{
      $id = $_GET['id'];

      if(isset($_GET['id'])):
        $sql = 'SELECT * FROM doa_pilihan WHERE id = ' . $id;
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        header('Content-Type: text/html; charset=utf-8');
        echo json_encode(
          [["stat"=>"true"],["data"=>$result]],
          JSON_UNESCAPED_UNICODE);
      else:
        echo json_encode(
          [["stat"=>"false"],["data"=>"This table need id parameter"]],
          JSON_UNESCAPED_UNICODE);
      endif;
    }catch(PDOException $err){
      echo json_encode(
        [["stat"=>"false"], ["data"=>$err->getMessage()]]);
    }
  }

  function getInfoKelas(){
    try{
      $kaderID=$_GET['kaderId'];

      $sql = 'SELECT * FROM qr_info_kelas where kader_id = "'.$kaderID.'"';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );
      $json = json_encode( $result );
      
      echo $json;
    }catch(PDOException $err){
      echo $err;
    }
  }

  function getRapors(){
    try{
      $tahun=$_GET['tahun']; 
      $bulan=$_GET['bulan'];

      $sql = 'SELECT * FROM qr_rekap_rapor_by_bulantahun where tahun = "'.$tahun.'" AND bulan = "'.$bulan.'"';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );
      $json = json_encode( $result );
      
      echo $json;
    }catch(PDOException $err){
      echo $err;
    }
  }
  function getRapor(){
    try{
      $kaderId=$_GET['kaderId'];

      $sql = 'SELECT * FROM qr_rekap_rapor_by_bulantahunmatkul where kader_id = "'.$kaderId.'"';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

      $json = json_encode( $result );
      echo $json;
    }catch(PDOException $err){
      echo $err;
    }
  }
  function getRaporId(){
    try{
      $kaderID=$_GET['kaderId'];

      $sql = 'SELECT * FROM qr_rekap_rapor_by_bulantahun where kader_id = "'.$kaderID.'"';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

      $json = json_encode( $result );
      echo $json;
    }catch(PDOException $err){
      echo $err;
    }
  }
}

$amaliyah = new Amaliyah();
$mode = $_GET['mode'];

switch ($mode) {
  case 'insert':
    $amaliyah->insert();
  break;

  case 'surah':
    $amaliyah->getSurah();
  break;

  case 'hadits':
    $amaliyah->getHadits();
  break;

  case 'amal':
    $amaliyah->getAmal();
  break;

  case 'rekap_tgl':
    $amaliyah->getAktifitasByTgl();
  break;

  case 'rekap_tgl_one':
    $amaliyah->getAktifitasByTglOne();
  break;

  case 'get_aktifitas_tgl':
    $amaliyah->getAktifitasTgl();
  break;

  case 'get_aktifitas_materi':
    $amaliyah->getAktifitasByMateri();
  break;

  // zikir pagi & petang
  case 'get_zikirpp_list':
    $amaliyah->getZikirPP();
  break;
  case 'get_zikirpp_detail':
    $amaliyah->getZikirPPDetail();
  break;

  // zikir sholat
  case 'get_zsholat_list':
    $amaliyah->getZikirSholat();
  break;
  case 'get_zsholat_detail':
    $amaliyah->getZikirSholatDetail();
  break;

  case 'get_list_ebook':
    $amaliyah->getListEbook();
  break;
  case 'get_ebook':
    $amaliyah->getEbook();
  break;

  case 'get_list_doa':
    $amaliyah->getListDoa();
  break;
  case 'get_doa':
    $amaliyah->getDoa();
  break;

  case 'get_info_kelas':
    $amaliyah->getInfoKelas();
  break;
  case 'get_rapors':
    $amaliyah->getRapors();
  break;
  case 'get_rapor':
    $amaliyah->getRapor();
  break;
  case 'get_rapor_id':
    $amaliyah->getRaporId();
  break;
}

?>
