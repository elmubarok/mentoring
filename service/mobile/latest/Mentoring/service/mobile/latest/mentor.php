<?php

include_once 'config.php';

class Mentor{
  private $conn;
  private $db;

  function __construct(){
    $this->conn = new Config();
    $this->db = $this->conn->openConnection();
  }

  function approve(){
    $kader_id=$_GET['kaderId'];
    $materi_id=$_GET['materiId'];
    $tgl = $_GET['tgl'];

    try{
      $sql = 'UPDATE kuliah set is_approved=1 where 
            kader_id = "'.$kader_id.'" AND 
            materi_id = "'.$materi_id.'" AND
            tgl = "'.$tgl.'"';
      $stmt = $this->db->prepare($sql);
      $stmt->execute();

      echo json_encode([["stat"=>"true"]]);
    }catch(PDOException $err){
      echo json_encode([["stat"=>"false"]]);
    }
  }

  function getKelasMentor(){
    $mentor_id=$_GET['mentorId'];

    $sql = 'SELECT * FROM qr_kelas where mentor_id = "' . $mentor_id. '"';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

    header('Content-Type: text/html; charset=utf-8');
    echo json_encode(
      [["stat"=>"true"],["data"=>$result]],
      JSON_UNESCAPED_UNICODE);
  }

  function getKaderMentor(){
    $kelas_id = $_GET['kelasId'];

    $sql = 'SELECT * FROM qr_aktivitas_noapproved where kelas_id = "' . $kelas_id. '"';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    $result = $stmt->fetchAll( PDO::FETCH_ASSOC );
    echo json_encode(
      [["stat"=>"true"],["data"=>$result]],
      JSON_UNESCAPED_UNICODE);
  }

  function getTugasMentor(){
    $kelas_id = $_GET['kelasId'];
    $tgl = $_GET['tgl'];

    $sql = 'SELECT * FROM qr_aktivitas_noapproved_d where kelas_id = "' . $kelas_id. '"
    AND tgl = "'.str_replace("/", "-", $tgl).'" ORDER BY is_surat DESC';

    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    $result = $stmt->fetchAll( PDO::FETCH_ASSOC );
    echo json_encode(
      [["stat"=>"true"],["data"=>$result]],
      JSON_UNESCAPED_UNICODE);
  }

}

$mentor = new Mentor();
$mode = $_GET['mode'];

switch ($mode) {
  case 'kelas_mentor':
    $mentor->getKelasMentor();
  break;
  case 'kader_mentor':
    $mentor->getKaderMentor();
  break;
  case 'tugas_mentor':
    $mentor->getTugasMentor();
  break;
  case 'approve':
    $mentor->approve();
  break;
}

?>
