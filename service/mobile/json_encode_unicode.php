<?php

$data = (object)array(
	"html" => "<foo bar=\"baz\"/> &amp;",
	"arabic" => "العربية al-ʿarabiyyah, IPA: [æl ʕɑrɑˈbijjɐ], or عربي ʿarabī",
	"hebrew" => "עִבְרִית, Ivrit",
	"chinese" => "汉语/漢語 Hanyu; 华语/華語 Huáyǔ; 中文 Zhōngwén",
	"korean" => "한국어/조선말",
	"japanese" => "日本語 Nihongo",
	"umlauts" => "äüöãáàß",
	"escaped" => "\u65e5\u672c\u8a9e",
	"emoji" => json_decode('"\u263a \ue415\ue056\ue057\ue414\ue405\ue106\ue418 \ud83d\ude04\ud83d\ude0a\ud83d\ude03\ud83d\ude09\ud83d\ude0d\ud83d\ude18"'),
);

header("Content-Type: text/plain; charset=UTF-8");

print title("php: print_r(data)")."\n".print_r($data,true)."\n\n";

print title("php: json_encode(data)")."\n".($json = json_encode($data))."\nstrlen: ".strlen($json)."\n\n\n";

print title("php: json_encode_unicode(data)")."\n".($json = json_encode_unicode($data))."\nstrlen: ".strlen($json)."\n\n\n";

print title("php: print_r(json_decode(json_encode_unicode(data)))")."\n".print_r(json_decode(json_encode_unicode($data)),true)."\n\n";

print title("php: data == json_decode(json_encode_unicode(data))")."\n".print_r($data == json_decode(json_encode_unicode($data)),true)."\n\n";

function json_encode_unicode($data) {
	if (defined('JSON_UNESCAPED_UNICODE')) {
		return json_encode($data, JSON_UNESCAPED_UNICODE);
	}
	return preg_replace_callback('/(?<!\\\\)\\\\u([0-9a-f]{4})/i',
		function ($m) {
			$d = pack("H*", $m[1]);
			$r = mb_convert_encoding($d, "UTF8", "UTF-16BE");
			return $r!=="?" && $r!=="" ? $r : $m[0];
		}, json_encode($data)
	);
}


// some benchmarking...

print "\n\n".title("Benchmarks")."\n";
gauge("json_encode", 1000, function() use ($data) {
	json_encode($data);
});
gauge("json_encode_unicode", 1000, function() use ($data) {
	json_encode_unicode($data);
});

// Utils...

function gauge($label, $times, $callback) {
	$tstart = microtime(true);
	for ($i=0; $i<$times; $i++) {
		$callback();
	}
	$tend = microtime(true);
	$took = ($tend-$tstart);
	print (ceil($took*1000)/1000)." to run $label run $times times\n";
}

function title($str) {
	return $str."\n".str_repeat("-",strlen($str));
}

?>